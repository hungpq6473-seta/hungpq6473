-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2014 at 02:00 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `launch_yii`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inventory`
--

CREATE TABLE IF NOT EXISTS `tbl_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tenant_dbu` varchar(255) NOT NULL,
  `tenant_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Triggers `tbl_inventory`
--
DROP TRIGGER IF EXISTS `tr_inventory_before_insert`;
DELIMITER //
CREATE TRIGGER `tr_inventory_before_insert` BEFORE INSERT ON `tbl_inventory`
 FOR EACH ROW thisTrigger: BEGIN
	IF (SUBSTRING_INDEX(USER(),'@',1) = 'root')
     THEN
       LEAVE thisTrigger;
     END IF;
     SET new.tenant_dbu = SUBSTRING_INDEX(USER(),'@',1);
    END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_live_event`
--

CREATE TABLE IF NOT EXISTS `tbl_live_event` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `title_event` varchar(355) DEFAULT NULL,
  `description_event` text,
  `create_by` int(11) NOT NULL DEFAULT '0',
  `location` varchar(255) DEFAULT NULL,
  `tenant_id` int(11) NOT NULL DEFAULT '0',
  `tenant_dbu` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_live_event`
--

INSERT INTO `tbl_live_event` (`eid`, `title_event`, `description_event`, `create_by`, `location`, `tenant_id`, `tenant_dbu`, `status`, `created`) VALUES
(1, 'Event Demo 01', 'Description Event Demo 01', 1, 'Hanoi - Vietnam', 1, 'test', 1, 0),
(2, 'Event Demo 02', 'Description Event Demo 02', 1, 'Tay Ho - Nanoi - Vietnam', 1, 'test', 1, 0),
(3, 'Event Demo 03', 'Description Event Demo 03', 1, 'Cau Giay - Hanoi - Vietnam', 1, 'test', 1, 0),
(4, 'Event Demo 04', 'Description Event Demo 04', 2, 'Hai Phong - Vietnam', 2, 'tenant1', 1, 0),
(5, 'Live Event Demo 01', 'Live Event Description Demo 01', 1, 'My Dinh - Tu Liem - Hanoi - Vietnam', 1, 'test', 1, 0),
(6, 'Live Event Demo 01', 'Live Event Description Demo 01', 1, 'My Dinh - Tu Liem - Hanoi - Vietnam', 1, 'test', 1, 0),
(7, 'Live Event Demo 10', 'Live Event Description Demo 10', 1, 'My Dinh - Tu Liem - Hanoi - Vietnam', 1, 'tenant1', 1, 0);

--
-- Triggers `tbl_live_event`
--
DROP TRIGGER IF EXISTS `tr_live_event_before_insert`;
DELIMITER //
CREATE TRIGGER `tr_live_event_before_insert` BEFORE INSERT ON `tbl_live_event`
 FOR EACH ROW thisTrigger: BEGIN
	IF (SUBSTRING_INDEX(USER(),'@',1) = 'root')
     THEN
       LEAVE thisTrigger;
     END IF;
     SET new.tenant_dbu = SUBSTRING_INDEX(USER(),'@',1);
    END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `create_time` int(11) NOT NULL DEFAULT '1',
  `update_time` int(11) NOT NULL DEFAULT '1',
  `can_admin` int(1) NOT NULL DEFAULT '0' COMMENT '1: Can admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `name`, `create_time`, `update_time`, `can_admin`) VALUES
(1, 'Adminstrator', 1399976248, 1399976248, 1),
(2, 'Moderator', 1399976248, 1399976248, 1),
(3, 'Authentication', 1399976248, 1399976248, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tenant`
--

CREATE TABLE IF NOT EXISTS `tbl_tenant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `dbu` varchar(255) DEFAULT NULL,
  `e_dbpwd` varbinary(1024) NOT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_tenant`
--

INSERT INTO `tbl_tenant` (`id`, `full_name`, `domain`, `dbu`, `e_dbpwd`, `business_name`, `status`, `created`) VALUES
(1, 'Name Demo 1', 'launch.alo', 'root', 'hd', 'Launch Projects', 1, 1399974519),
(2, 'Name Demo 2', 'partner.launch.alo', 'tenant1', '123456', 'Partner 01', 1, 1399972519),
(9, 'Name Demo 3', 'demo.launch.alo', 'test', 'test', 'Demo Multisite', 1, 1399974519),
(14, 'Pham Quoc Hung', 'seta.launch.alo', 'demo_data', 'hd', 'SETA', 1, 1401187235);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `new_email` varchar(255) DEFAULT NULL,
  `tenant_dbu` varchar(255) DEFAULT NULL,
  `tenant_id` int(11) NOT NULL DEFAULT '0',
  `tenant_owner` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(1) NOT NULL DEFAULT '3',
  `user_type` tinyint(3) DEFAULT '0',
  `auth_key` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0: unpublic, 1: public',
  `created` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `full_name`, `email`, `new_email`, `tenant_dbu`, `tenant_id`, `tenant_owner`, `password`, `role_id`, `user_type`, `auth_key`, `status`, `created`) VALUES
(1, 'admin', 'Admin', 'admin@gmail.com', NULL, 'root', 1, 1, '0ea2abc47f79c49f460d7573aacb529e47d8147c72a543b6eea0c1de280a277efc51ddf013d47e12109cc7b8053727176970c4ada94ae0cf47f3e3cac899bcdb', 1, 0, NULL, 1, 1401178162),
(5, 'demo', 'Demo Test 1', 'demo@gmail.com', NULL, 'test', 9, 1, 'a86e5b66514632d2d20963dd4d87df21f7237fdc157c95e97fbaf0167c755d22f524f9c9874f41769562bc9b2c8aefef283f4c171b3eb32b9b4e598afd3ff25f', 3, 0, NULL, 1, 1401178162),
(6, 'partner', 'Launch Three', 'partner@gmail.com', NULL, 'tenant1', 2, 5, '2de4bf8c1b92d5fa116b69dea57430b15b8bfdd62df379f128987b87855653d1637fd942389f1f8a09793fbd628310714fdf45c14d86027eda3dcec42fa543a2', 3, 0, NULL, 1, 1401178162),
(25, 'thumotti', 'Thu Mot Ti', 'thumotti@gmail.com', NULL, 'test', 9, 0, 'd9a2968dc6c572d22383d6d06c2d59ce93d7ebde2dbb027ffdfc09fdb3349fbfe2ee57b3a0255430cbd6c7bdebd2cf218faf08804907f70bbe2230fea6017825', 3, 0, NULL, 1, 1401178162),
(26, 'thuhaiti', 'Thu Hai Ti', 'thuhaiti@gmail.com', NULL, 'test', 9, 0, '62eac579dae44e491495beafe0d41709c38c2ef08d53a8a12a9cfed402d5bbdfaff7cc8ac92ff37f079bdda5083af7b523689cb1fb7e2aad569e544eb8ad97f5', 3, 0, NULL, 1, 1401178162),
(27, 'thubati', 'Thu Ba Ti', 'thubati@gmail.com', NULL, 'test', 9, 0, 'c00f1fb6bf176741448441dad94a65ced31e50902c0025be4c9daef21ca06dbc3b23dd9882caf142190cce867afccc5747ae0d4b322e394cf99d972fa1c2deca', 3, 0, NULL, 1, 1401178162);

--
-- Triggers `tbl_user`
--
DROP TRIGGER IF EXISTS `tr_user_before_insert`;
DELIMITER //
CREATE TRIGGER `tr_user_before_insert` BEFORE INSERT ON `tbl_user`
 FOR EACH ROW thisTrigger: BEGIN
	IF (SUBSTRING_INDEX(USER(),'@',1) = 'root')
     THEN
       LEAVE thisTrigger;
     END IF;
     SET new.tenant_dbu = SUBSTRING_INDEX(USER(),'@',1);
    END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_key`
--

CREATE TABLE IF NOT EXISTS `tbl_user_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(2) NOT NULL DEFAULT '0',
  `key` varchar(255) DEFAULT NULL,
  `created` int(11) NOT NULL DEFAULT '1',
  `consume_time` int(11) NOT NULL DEFAULT '1',
  `expire_time` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_user_key`
--

INSERT INTO `tbl_user_key` (`id`, `user_id`, `type`, `key`, `created`, `consume_time`, `expire_time`) VALUES
(8, 20, 1, 'XhigI6hdSa7wjZEb13ry3Ew39xfRjFlJ', 2014, 1, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_inventory`
--
CREATE TABLE IF NOT EXISTS `vw_inventory` (
`id` int(11)
,`item_number` varchar(255)
,`description` varchar(255)
,`tenant_dbu` varchar(255)
,`tenant_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_live_event`
--
CREATE TABLE IF NOT EXISTS `vw_live_event` (
`eid` int(11)
,`title_event` varchar(355)
,`description_event` text
,`create_by` int(11)
,`location` varchar(255)
,`tenant_id` int(11)
,`tenant_dbu` varchar(255)
,`status` int(1)
,`created` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `vw_tenant`
--
-- in use(#1356 - View 'launch_yii.vw_tenant' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)
-- Error reading data: (#1356 - View 'launch_yii.vw_tenant' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_user`
--
CREATE TABLE IF NOT EXISTS `vw_user` (
`id` int(11)
,`username` varchar(255)
,`full_name` varchar(255)
,`tenant_id` int(11)
,`tenant_dbu` varchar(255)
,`tenant_owner` tinyint(1)
,`password` varchar(255)
,`status` int(1)
,`created` int(11)
,`user_type` tinyint(3)
);
-- --------------------------------------------------------

--
-- Structure for view `vw_inventory`
--
DROP TABLE IF EXISTS `vw_inventory`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY INVOKER VIEW `vw_inventory` AS select `tbl_inventory`.`id` AS `id`,`tbl_inventory`.`item_number` AS `item_number`,`tbl_inventory`.`description` AS `description`,`tbl_inventory`.`tenant_dbu` AS `tenant_dbu`,`tbl_inventory`.`tenant_id` AS `tenant_id` from `tbl_inventory` where (`tbl_inventory`.`tenant_dbu` = substring_index(user(),'@',1));

-- --------------------------------------------------------

--
-- Structure for view `vw_live_event`
--
DROP TABLE IF EXISTS `vw_live_event`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY INVOKER VIEW `vw_live_event` AS select `tbl_live_event`.`eid` AS `eid`,`tbl_live_event`.`title_event` AS `title_event`,`tbl_live_event`.`description_event` AS `description_event`,`tbl_live_event`.`create_by` AS `create_by`,`tbl_live_event`.`location` AS `location`,`tbl_live_event`.`tenant_id` AS `tenant_id`,`tbl_live_event`.`tenant_dbu` AS `tenant_dbu`,`tbl_live_event`.`status` AS `status`,`tbl_live_event`.`created` AS `created` from `tbl_live_event` where (`tbl_live_event`.`tenant_dbu` = substring_index(user(),'@',1));

-- --------------------------------------------------------

--
-- Structure for view `vw_user`
--
DROP TABLE IF EXISTS `vw_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY INVOKER VIEW `vw_user` AS select `tbl_user`.`id` AS `id`,`tbl_user`.`username` AS `username`,`tbl_user`.`full_name` AS `full_name`,`tbl_user`.`tenant_id` AS `tenant_id`,`tbl_user`.`tenant_dbu` AS `tenant_dbu`,`tbl_user`.`tenant_owner` AS `tenant_owner`,`tbl_user`.`password` AS `password`,`tbl_user`.`status` AS `status`,`tbl_user`.`created` AS `created`,`tbl_user`.`user_type` AS `user_type` from `tbl_user` where (`tbl_user`.`tenant_dbu` = substring_index(user(),'@',1));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
