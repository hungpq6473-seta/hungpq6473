<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate() {
        global $tenant;
        $objTuser = new TUser();
        $tenantDbu = $tenant['dbu'];
        if (strpos($this->username, '@') !== false) {
            if (ST_DOMAIN == ST_CUR_DOMAIN)
                $cond = array('email' => $this->username);
            else
                $cond = array('email' => $this->username, 'tenant_dbu' => $tenantDbu);
        }else {
            if (ST_DOMAIN == ST_CUR_DOMAIN)
                $cond = array('username' => $this->username);
            else
                $cond = array('username' => $this->username, 'tenant_dbu' => $tenantDbu);
        }

        $users = TUser::model()->findByAttributes($cond);

        if (isset($users))
            $pass = $objTuser->validatePassword($this->password, $users->username);

        if (!isset($users))
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        elseif ($pass != $users->password)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $users->id;
            $this->setState('full_name', $users->full_name);
            $this->setState('name', $users->full_name);
            $this->setState('email', $users->email);
            $this->setState('username', $users->username);
            $this->setState('tenant_id', $users->tenant_id);
            $this->setState('company', $users->company);
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

    protected function loadUser($_id = null) {
        if ($this->_model === NULL) {
            if ($_id !== NULL) {
                $this->_model = TUser::model()->findByPk($_id);
            }
        }
        return $this->_model;
    }

}
