<?php
/**
 * Created by PhpStorm.
 * User: Pham Quoc Hung
 * Date: 6/5/14
 * Time: 11:30 AM
 */
class SEOUtil
{
    public static function metaSocial($link='',$title='', $desc='', $image='', $type=''){
        if($link){
            Yii::app()->clientScript->registerMetaTag($link,null,null,array('property'=>'og:url'));
        }
        if($title){
            Yii::app()->clientScript->registerMetaTag($title,null,null,array('property'=>'og:title'));
        }
        if($desc){
            Yii::app()->clientScript->registerMetaTag($desc,null,null,array('property'=>'og:description'));
        }
        if($image){
            Yii::app()->clientScript->registerMetaTag($image,null,null,array('property'=>'og:image'));
        }
        if($type){
            Yii::app()->clientScript->registerMetaTag($type,null,null,array('property'=>'og:type'));
        }
    }

}