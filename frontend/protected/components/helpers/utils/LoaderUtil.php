<?php
/**
 * Created by PhpStorm.
 * User: Pham Quoc Hung
 * Date: 6/5/14
 * Time: 10:30 AM
 */
class LoaderUtil
{
    public static $path;
    public static $theme;
    public static function load($type, $files){
        if($type == 'theme'){
            $theme = Yii::app()->theme->name;
            self::$theme = $theme;
            self::$path = Yii::app()->theme->baseUrl;
        }else{
            self::$path = Yii::app()->request->baseUrl;
        }

        $js = array();
        $css = array();

        foreach ($files as $key=>$file){
            $ext = explode('.', $file);
            switch (strtolower(end($ext))){
                case 'js': 	$js[] 	= $file; break;
                case 'css': $css[]	= $file; break;
                default: break;
            }
        }
        self::loadJs($js);
        self::loadCss($css);
    }

    private static function loadCss($css) {
        foreach($css as $item){
            Yii::app()->clientScript->registerCssFile(self::$path.'/'.$item, 'screen', CClientScript::POS_HEAD);
        }
    }
    private static function loadJs($js) {
        foreach($js as $item){
            Yii::app()->clientScript->registerScriptFile(self::$path.'/'.$item, CClientScript::POS_END);
        }
    }
}