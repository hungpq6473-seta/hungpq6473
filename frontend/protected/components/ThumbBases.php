<?php

class ThumbBases
{
    function __construct(){
        $path = Yii::app()->basePath.'/extensions/phpthumb';
        if( !defined('PhpThumbFactoryLoaded') ) {
            require_once $path . '/ThumbLib.inc.php';
            define('PhpThumbFactoryLoaded',1);
        }
        if( !defined('SetaCoreImageBase') ) {
            require_once $path . '/BaseSource.php';
            define('SetaCoreImageBase',1);
        }
    }
    public static function Thumbs($pathImage='', $imageWidth=200, $imageHeight=200, $title='', $isThumb=true, $image_quanlity = 100, $returnPath = false,$id = ""){
        $imageBases = new SetaCoreImageBase();
        $imageThumbBase = $imageBases->renderThumb( $pathImage, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false, $id );
        return $imageThumbBase;
    }
    public function ThumbsBase($pathImage='', $imageWidth=200, $imageHeight=200, $title='', $isThumb=true, $image_quanlity = 100, $returnPath = false,$id = ""){
        $imageBases = new SetaCoreImageBase();
        $imageThumbBase = $imageBases->renderThumbBase( $pathImage, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false, $id );
        return $imageThumbBase;
    }
    public function ThumbsResize( $pathImage='', $imageWidth=200, $imageHeight=200, $title='', $isThumb=true,  $image_quanlity = 100, $returnPath = false ){
        $imageBase = new SetaCoreImageBase();
        $imageThumbs = $imageBase->renderThumbResize( $pathImage, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false );
        return $imageThumbs;
    }
    public function ThumbsPercent( $pathImage='', $percent=100, $imageWidth=200, $imageHeight=200, $title='', $isThumb=true,  $image_quanlity = 100, $returnPath = false ){
        $imageBase = new SetaCoreImageBase();
        $imageThumbs = $imageBase->renderThumbPercent( $pathImage, $percent, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false );
        return $imageThumbs;
    }
    public function ThumbsCrop( $pathImage='', $crop1=100, $crop2=100, $imageWidth=200, $imageHeight=200, $title='', $isThumb=true,  $image_quanlity = 100, $returnPath = false ){
        $imageBase = new SetaCoreImageBase();
        $imageThumbs = $imageBase->renderThumbCrop( $pathImage, $crop1, $crop2, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false );
        return $imageThumbs;
    }
    public function ThumbsCropCenter( $pathImage='', $imageWidth=200, $imageHeight=200, $title='', $isThumb=true,  $image_quanlity = 100, $returnPath = false ){
        $imageBase = new SetaCoreImageBase();
        $imageThumbs = $imageBase->renderThumbCropCenter( $pathImage, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false );
        return $imageThumbs;
    }
    public function ThumbsCropFrom( $pathImage='', $crop1=100, $crop2=100, $imageWidth=200, $imageHeight=200, $title='', $isThumb=true,  $image_quanlity = 100, $returnPath = false ){
        $imageBase = new SetaCoreImageBase();
        $imageThumbs = $imageBase->renderThumbCropFrom( $pathImage, $crop1, $crop2, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false );
        return $imageThumbs;
    }
    public function ThumbsRotate( $pathImage='', $imageWidth=200, $imageHeight=200, $title='', $isThumb=true,  $image_quanlity = 100, $returnPath = false ){
        $imageBase = new SetaCoreImageBase();
        $imageThumbs = $imageBase->renderThumbRotate( $pathImage, $imageWidth, $imageHeight, $title, $isThumb=true,  $image_quanlity = 100, $returnPath = false );
        return $imageThumbs;
    }
}