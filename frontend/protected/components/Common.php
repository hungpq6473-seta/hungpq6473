<?php
    class Common extends CComponent
    {
        public static function checkMySQLUserExists($uname)
        {
            $sql = "SELECT user FROM mysql.user WHERE user = :user";
            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(":user", $uname, PDO::PARAM_STR);
            $dataReader=$command->query();
            return ($dataReader->rowCount == 0) ? false : true;
        }
        public static function createMySQLUser($uname, $upwd) // needs hardening against db errors
        {
            // create user
            $sql1 = "CREATE USER :uname@'localhost' IDENTIFIED BY :upwd";
            $command = Yii::app()->db->createCommand($sql1);
            $command->bindParam(":uname", $uname, PDO::PARAM_STR);
            $command->bindParam(":upwd", $upwd, PDO::PARAM_STR);
            $command->execute();
            // grant priviledges according to your needs
            $sql2 = "GRANT SELECT, INSERT, UPDATE, DELETE, TRIGGER, SHOW VIEW, EXECUTE ON launch_yii.* TO :uname@'localhost'";
            $command = Yii::app()->db->createCommand($sql2);
            $command->bindParam(":uname", $uname, PDO::PARAM_STR);
            $command->execute();
            // update tables
            $sql3 = "FLUSH PRIVILEGES";
            $command = Yii::app()->db->createCommand($sql3);
            $command->execute();
        }
    }