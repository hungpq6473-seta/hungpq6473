<?php

/**
 * Oauth provider based on Hybrid Auth and CUserIndentity
 * @author An Dinh Luan <luanad6365@setacinq.com.vn>
 * @package Launch
 */
class Oauth extends UserIdentity {

	public $adapter = null;
	public $auth = null;
	public $userProfile = null;
	public $allowedProviders = array('google', 'facebook', 'twitter',);
	public $config;
	public static $_instance = null;

	public function __construct($callbackUrl = null) {

		$path = Yii::getPathOfAlias('ext.hybridauth');
		require_once $path . '/Hybrid/Auth.php';
		require_once $path . '/Hybrid/Endpoint.php';

		//"base_url" => ""http://launch.com/user/socialLogin" 

		$this->config = Yii::app()->params['oauth'];
		if ($callbackUrl) {
			$this->config['base_url'] = $callbackUrl;
		}
		// create an instance for Hybridauth with the configuration file path as parameter
		$this->auth = new Hybrid_Auth($this->config);
	}

	public static function getInstance($callbackUrl = null) {
		if (!self::$_instance) {
			self::$_instance = new self($callbackUrl);
		}

		return self::$_instance;
	}

	/**
	 * Try to authenticate the user with a given provider
	 * @param string $provider
	 * @return object Hybrid Adapter
	 */
	public function authenticateSocial($provider = 'facebook', $params = array()) {
		// try to authenticate the selected $provider
		$this->adapter = $this->auth->authenticate($provider, $params);
		// grab the user profile
		$this->userProfile = $this->adapter->getUserProfile();
		return $this->adapter;
	}

	public function processEndpoint() {
		Hybrid_Endpoint::process();
		exit;
	}

	public function logout($all = false) {
		if ($all) {
			Hybrid_Auth::logoutAllProviders();
		}
	}

	/**
	 * Get the session stored data
	 * @return array serilized
	 */
	public function getSessionData() {
		return Hybrid_Auth::storage()->getSessionData();
	}

	/**
	 * Restore HybridAuth session from a given [serialized] array
	 */
	public function restoreSessionData($data) {
		Hybrid_Auth::storage()->restoreSessionData($data);
	}

	/**
	 * Update the user status or post to his wall across social networks.
	 * Currently only Facebook, Twitter, Identica, LinkedIn, QQ, Sina, Murmur, Pixnet and Plurk do support this feature.
	 * Others providers will throw an exception (#8 "Provider does not support this feature"), when setUserStatus() is called.
	 * @param string $status
	 */
	public function setUserStatus($status) {
		$this->adapter->setUserStatus($status);
	}

	/**
	 * Return an instance of the provider api if any
	 * Ref: http://hybridauth.sourceforge.net/userguide/Access_IDps_APIs.html
	 * @return type
	 */
	public function getApi() {
		return $this->adapter->api();
	}

	public function validateProviderName($provider) {
		if (!is_string($provider))
			return false;
		if (!in_array($provider, $this->allowedProviders))
			return false;

		return true;
	}

	/**
	 * Generate new Username for Social User using first name and last name of Social User
	 * @return string new username
	 */
	public function generateUsername($firstName, $lastName, $type = '') {
		$dot = ".";
		$rand_number = rand(2, 9999);
		$username = "";
		$firstName = str_replace(' ', $dot, $firstName);
		$lastName = str_replace(' ', $dot, $lastName);
		switch ($type) {
			case "LAST_FIRST":
				$username = $lastName == "" ? "" : $username . self::stripText($lastName);
				$username .= $firstName == "" ? "" : $dot . self::stripText($$firstName);
				break;
			case "ONLY_FIRST":
				$username = $firstName == "" ? $username : self::stripText($firstName);
				break;
			case "ONLY_LAST":
				$username = $lastName == "" ? "" : $username . self::stripText($lastName);
				break;
			default :
				$username = $firstName == "" ? $username : self::stripText($firstName);
				$username .= $lastName == "" ? "" : $dot . self::stripText($lastName);
		}

		$username .= $dot . $rand_number;
		return strtolower($username);
	}

    public static function stripText($string) {
        $from = array("à", "ả", "ã", "á", "ạ", "ă", "ằ", "ẳ", "ẵ", "ắ", "ặ", "â", "ầ", "ẩ", "ẫ", "ấ", "ậ", "đ", "è", "ẻ", "ẽ", "é", "ẹ", "ê", "ề", "ể", "ễ", "ế", "ệ", "ì", "ỉ", "ĩ", "í", "ị", "ò", "ỏ", "õ", "ó", "ọ", "ô", "ồ", "ổ", "ỗ", "ố", "ộ", "ơ", "ờ", "ở", "ỡ", "ớ", "ợ", "ù", "ủ", "ũ", "ú", "ụ", "ư", "ừ", "ử", "ữ", "ứ", "ự", "ỳ", "ỷ", "ỹ", "ý", "ỵ", "À", "Ả", "Ã", "Á", "Ạ", "Ă", "Ằ", "Ẳ", "Ẵ", "Ắ", "Ặ", "Â", "Ầ", "Ẩ", "Ẫ", "Ấ", "Ậ", "Đ", "È", "Ẻ", "Ẽ", "É", "Ẹ", "Ê", "Ề", "Ể", "Ễ", "Ế", "Ệ", "Ì", "Ỉ", "Ĩ", "Í", "Ị", "Ò", "Ỏ", "Õ", "Ó", "Ọ", "Ô", "Ồ", "Ổ", "Ỗ", "Ố", "Ộ", "Ơ", "Ờ", "Ở", "Ỡ", "Ớ", "Ợ", "Ù", "Ủ", "Ũ", "Ú", "Ụ", "Ư", "Ừ", "Ử", "Ữ", "Ứ", "Ự", "Ỳ", "Ỷ", "Ỹ", "Ý", "Ỵ");
        $to = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "d", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "D", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y");
        return str_replace($from, $to, $string);
    }

	/**
	 * After authenticate social, we need make user login to our system
	 * with exists user in our database
	 */
	public function makeLogin($identifier, $username) {
		$model = new LoginForm;
		$model->username = $username;
		$model->password = $identifier;
		if ($model->login()) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Redirect to user domain 
	 */

	public function getRedirect($tenantId = '') {
		if(!$tenantId){
			$tenantId = Yii::app()->user->tenant_id;
		}
		$domain = TTenant::model()->find(array(
			'select' => 'domain',
			'condition' => 'id=:tenant_id',
			'params' => array(':tenant_id' => $tenantId),
		));
		return 'http://' . $domain->domain;
	}

}
