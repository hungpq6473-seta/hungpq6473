<?php

class AppStartup extends CBehavior {

    public function events() {

        return array_merge(parent::events(), array(
            'onBeginRequest' => 'beginRequest',
        ));
    }

    public function init() {
        global $tenant;
        if (ST_DOMAIN == ST_CUR_DOMAIN) {
            $lang = $this->getLangUrl();
            Yii::app()->setLanguage($lang);
        } else {
            Yii::app()->setLanguage($tenant['language']);
        }
        $bases = array(
            'baseUrl' => Yii::app()->getBaseUrl(true),
            'themeUrl' => Yii::app()->theme->baseUrl
        );
        $params = 'PARAMS = ' . json_encode($bases);
        Yii::app()->clientScript->registerScript('BASE_DATA', $params, CClientScript::POS_END);

        $loadFiles = new LoaderUtil();
        $files = array(
            'css/libs/bootstrap-and-responsive.min.css',
            'css/styles.css',
            'js/libs/modernizr-2.5.3-respond-1.1.0.min.js',
            'js/libs/bootstrap/bootstrap.min.js',
            'js/libs/jquery.html5uploader.cus.js',
            'js/STAjax.js',
            'js/STQuery.js',
        );
        $loadFiles->load('root', $files);
    }

    public function beginRequest() {
        $domain = $this->getInfoDomain();
        $dataDomain = $this->getDataDomain($domain);
        if ($domain != ST_DOMAIN) {
            $tu = $dataDomain->dbu;
            $tp = str_replace('-launch','',base64_decode(base64_decode($dataDomain->e_dbpwd)));
            $this->setActiveDB($tu, $tp);
            Yii::app()->theme = $dataDomain->theme;
        } else {
            // switch db credentials for logged in users
            if (!Yii::app()->user->isGuest) {
                $u = TUser::model()->findByPk(Yii::app()->user->id);
                $tu = TTenant::model()->findByPk($u->tenant_id)->dbu;
                $tp = TTenant::model()->findByPk($u->tenant_id)->e_dbpwd;
                $tp = str_replace('-launch','',base64_decode(base64_decode($tp)));
                $this->setActiveDB($tu, $tp);
            }
        }
        $this->init();
        //For google provider in Oauth
        $this->googleLoginAfterRedrect();
        $this->_getUsers();
    }

    /**
     * Switch Active Database
     * */
    public function setActiveDB($tu, $tp) {
        Yii::app()->db->setActive(false);
        Yii::app()->db->username = $tu;
        Yii::app()->db->password = $tp;
        Yii::app()->db->setActive(true);
    }

    public function getDataDomain($domain) {
        $infos = TTenant::model()->findByAttributes(array('domain' => $domain));

        $GLOBALS["tenant"] = array(
            'tenant_id' => $infos->id,
            'domain' => $infos->domain,
            'dbu' => $infos->dbu,
            'business_name' => $infos->business_name,
            'theme' => $infos->theme,
            'layout' => $infos->layout,
            'language' => $infos->language,
        );
        return $infos;
    }

    /**
     * Get HTTP/HTTPS (the possible values for this vary from server to server)
     * Get domain portion
     * */
    public function getInfoDomain() {
        $myUrl = $_SERVER['HTTP_HOST'];
        return $myUrl;
    }

    /**
     * get local language selected
     * */
    public function getLangUrl() {
        global $language;

        $langDefault = Yii::app()->sourceLanguage;
        $langName = $langDefault;
        $language = $langDefault;
        $languages = Yii::app()->params->languages;
        $url = Yii::app()->getRequest()->requestUri;
        foreach ($languages as $key => $lang) {
            $langPrex = "(/" . $key . "/|/" . $key . "$)";
            $langCheck = preg_match($langPrex, $url);
            if ($langCheck == 1) {
                $language = $key;
            }
        }
        if ($language == $langDefault) {
            $language = '';
        } else {
            $langName = $language;
            $language = '/' . $language;
        }
        return $langName;
    }

    public function googleLoginAfterRedrect() {
        if (isset($_COOKIE['send_back_login'])) {
            $data = json_decode($_COOKIE['send_back_login'], true);
            $model = new LoginForm;
            $model->username = $data['username'];
            $model->password = $data['identifier'];
            //Delete cookies sent from Launch.com
            $expire = time() - 60 * 30;
            setcookie("send_back_login", json_encode($_COOKIE['send_back_login']), $expire, "/", ST_CUR_DOMAIN, 0, true);
            $model->login();
            //Always logout in Launch.com, only keep login in partner site
            if (ST_DOMAIN == ST_CUR_DOMAIN){
                Yii::app()->user->logout();
            }
            Yii::app()->request->redirect('/');
        }
    }

    public function _getUsers(){
        global $user;
        if(Yii::app()->user->id){
            $data_fields = array(
                'id'=>0,
                'username'=>'',
                'full_name'=>'',
                'company'=>'',
                'company_type'=>0,
                'email'=>'',
                'tenant_dbu'=>'',
                'tenant_id'=>0,
                'tenant_owner'=>0,
                'account_type'=>'',
                'bio_user'=>'',
                'avatar'=>'',
                'role_id'=>3,
                'user_type'=>0,
                'auth_key'=>'',
                'keep_private'=>1,
                'status'=>1,
                'created'=>1,

            );

            $criteria=new CDbCriteria;
            $criteria->select= array_keys($data_fields);  // only select the 'title' column
            $criteria->condition='id=:uid';
            $criteria->params=array(':uid'=>Yii::app()->user->id);
            $result=TUser::model()->find($criteria); // $params is not needed
            $data = $result->attributes;
            foreach($data_fields as $field=>$value){
                $user[$field] = !empty($data) && array_key_exists($field, $data) ? $data[$field] : $value;
            }
        }
    }

}
