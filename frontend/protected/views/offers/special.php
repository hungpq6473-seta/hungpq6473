<?php
/* @var $this OffersController */

$this->breadcrumbs=array(
    'Special Offers',
);
?>
<div id="special-offers" class="l-container container">
    <div class="main-special-offers">
        <div class="wrapper-special-offers">
            <div class="header-special-offers-page">
                <h3><?php echo Yii::t('_yii','Special Offers');?></h3>
                <div class="info-search-filter">
                    <form name="search-special-offers" id="search-special-offers" action="" method="get">
                        <div class="filter-sort">
                            <label><?php echo Yii::t('_yii','Sort by:');?></label>
                            <select name="sort" id="sortbook">
                                <option value="title-asc"><?php echo Yii::t('_yii','Title(A-Z)');?></option>
                                <option value="title-desc"><?php echo Yii::t('_yii','Title(Z-A)');?></option>
                            </select>
                        </div>
                        <div class="filter-search">
                            <div>
                                <label><?php echo Yii::t('_yii','Search:');?></label>
                                <input type="text" name="key" class="key-search-input" value="<?php echo isset($_GET['key'])?$_GET['key']:'';?>">
                                <button type="submit" class="submit-search" id="submit-search"><?php echo Yii::t('_yii','Search');?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="content-special-offers-page">
                <ul>
                    <?php //$this->renderPartial('_item');?>
                    <?php foreach( $offers as $key=>$item ):$item=(object)$item;?>
                        <li class="item-special-offers">
                            <div class="special-offers-image">
                                <a href="#" title="Launch! The Critical 90 Days from Idea to Market"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/offers/offers.png" alt="" /></a>
                            </div>
                            <div class="special-offers-content">
                                <div class="title-item-special-offers"><a href="#" title="<?php echo $item->title;?>"><?php echo $item->title;?></a></div>
                                <div class="author-item-special-offers"><strong><?php echo Yii::t('_yii', 'Category');?>:</strong><a href="#">Business Software</a></div>
                                <div class="content-item-special-offers">
                                    <?php echo $item->description;?>
                                    <a href="#" class="view-more-item"><?php echo Yii::t('_yii', 'more');?></a>
                                </div>
                                <a href="#" class="claim-button"><?php echo Yii::t('_yii','Claim This Offer');?></a>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="special-offers-footer">
                <a href="#" class="book-load-more"><?php echo Yii::t('_yii','View more');?></a>
            </div>
        </div>
    </div>
</div>