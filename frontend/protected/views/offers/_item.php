<li class="item-special-offers">
    <div class="special-offers-image">
        <a href="#" title="Launch! The Critical 90 Days from Idea to Market"><img src="<?php echo Yii::app()->request->baseUrl;?>/images/files/offers/offers.png" alt="" /></a>
    </div>
    <div class="special-offers-content">
        <div class="title-item-special-offers"><a href="#" title="Launch! The Critical 90 Days from Idea to Market">15% off Quicken Starter Edition from intuit</a></div>
        <div class="author-item-special-offers"><strong><?php echo Yii::t('_yii', 'Category');?>:</strong><a href="#">Business Software</a></div>
        <div class="content-item-special-offers">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            <a href="#" class="view-more-item"><?php echo Yii::t('_yii', 'more');?></a>
        </div>
        <a href="#" class="claim-button"><?php echo Yii::t('_yii','Claim This Offer');?></a>
    </div>
</li>