<?php
/* @var $this BookstoreController */

$this->breadcrumbs=array(
	'Bookstore',
);
?>
<div id="bookstore" class="l-container container">
    <div class="main-bookstore">
        <div class="wrapper-bookstore">
            <div class="header-bookstore-page">
                <h3><?php echo Yii::t('_yii','The Bookstore');?></h3>
                <div class="info-search-filter">
                    <form name="search-book" id="search-book" action="" method="get">
                        <div class="filter-sort">
                            <label><?php echo Yii::t('_yii','Sort by:');?></label>
                            <select name="sort" id="sortbook">
                                <option value="title-asc"><?php echo Yii::t('_yii','Title(A-Z)');?></option>
                                <option value="title-desc"><?php echo Yii::t('_yii','Title(Z-A)');?></option>
                            </select>
                        </div>
                        <div class="filter-search">
                            <div>
                                <label><?php echo Yii::t('_yii','Search:');?></label>
                                <input type="text" name="key" class="key-search-input" value="<?php echo isset($_GET['key'])?$_GET['key']:'';?>">
                                <button type="submit" class="submit-search" id="submit-search"><?php echo Yii::t('_yii','Search');?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="content-bookstore-page">
                <ul>
                    <?php $this->renderPartial('_item');?>
                </ul>
            </div>
            <div class="bookstore-footer">
                <a href="#" class="book-load-more"><?php echo Yii::t('_yii','View more');?></a>
            </div>
        </div>
    </div>
</div>