<?php
/* @var $this BookstoreController */

$this->breadcrumbs=array(
    'Bookstore Detail',
);
?>
<div id="bookstore-detail">
    <div class="bookstore-wrapper">
        <div class="bookstore-left-detail container">
            <div class="left-container l-container">
                <div class="left-book-detail-image">
                    <a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/files/book/Book_Item_Details.png" alt="" /></a>
                </div>
                <div class="left-book-pay">
                    <ul>
                        <li class="item-payment"><a href="#" class="amazon">Amazon</a></li>
                        <li class="item-payment"><a href="#" class="barnes_noble">Barnes & Noble</a></li>
                        <li class="item-payment"><a href="#" class="book_a_million">Book A Million</a></li>
                        <li class="item-payment"><a href="#" class="scottduffy">Scottduffy</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bookstore-right-detail container">
            <div class="right-container l-container">
                <div class="header-content-book">
                    <h3><?php echo "launch!: the critical 90 days from idea to market"?></h3>
                    <div id="share" class="social-book-detail">
                        <span class='st_email_large' displayText='Email'></span>
                        <a href="#" class="share-social plusone" data-social='{"type":"plusone", "url":"<?php echo Yii::app()->getBaseUrl(true).'/bookstore/detail'; ?>", "text": "<?php echo "launch!: the critical 90 days from idea to market"?>"}' title="<?php echo "launch!: the critical 90 days from idea to market"?>">Google Plus</a>
                        <a href="#" class='share-social twitter'  data-social='{"type":"twitter", "url":"<?php echo Yii::app()->getBaseUrl(true).'/bookstore/detail'; ?>", "text": "<?php echo "launch!: the critical 90 days from idea to market"?>"}'>Twitter</a>
                        <a href="#" class='share-social facebook' data-social='{"type":"facebook", "url":"<?php echo Yii::app()->getBaseUrl(true).'/bookstore/detail'; ?>", "text": "<?php echo "launch!: the critical 90 days from idea to market"?>"}'>Facebook</a>
                    </div>
                </div>
                <div class="main-content-book">
                    <div class="sum-book-detail">
                        <h4><?php echo YII::t('_yii','Book details');?></h4>
                        <div class="author-detail"><strong><?php echo Yii::t('_yii','Author');?>:</strong><a href="#">Scott Duffy</a></div>
                        <div class="publisher-detail"><strong><?php echo Yii::t('_yii','Publisher');?>:</strong><span>Portfolio Hardcover (March 20, 2014)</span></div>
                        <div class="category-detail"><strong><?php echo Yii::t('_yii','Category');?>:</strong><span>Business</span></div>
                    </div>
                    <div class="info-book-detail">
                        <h4><?php echo YII::t('_yii','Information');?></h4>
                        <div class="content-detail-info">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. </p>
                            <p>Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>