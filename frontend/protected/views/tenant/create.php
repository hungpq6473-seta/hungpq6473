<?php
/* @var $this UserController */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Create a site');
$this->breadcrumbs=array(
    Yii::t('_yii','Create a site'),
);
?>
<div id="create-user-account">
    <h3><?php echo Yii::t('_yii','Create a site');?></h3>

    <div class="form">
        <div class="well">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'create-tenant-form',
                'enableClientValidation'=>false,
                'errorMessageCssClass'=>'alert alert-error',
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>

            <div class="field-row">
                <?php echo $form->labelEx($model,'full_name'); ?>
                <?php echo $form->textField($model,'full_name'); ?>
                <?php echo $form->error($model,'full_name'); ?>
            </div>

            <div class="field-row">
                <?php echo $form->labelEx($model,'business_name'); ?>
                <?php echo $form->textField($model,'business_name'); ?>
                <?php echo $form->error($model,'business_name'); ?>
            </div>
            <div class="field-row">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email'); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
            <div class="field-row">
                <?php echo $form->labelEx($model,'dbu'); ?>
                <?php echo $form->textField($model,'dbu'); ?>
                <?php echo $form->error($model,'dbu'); ?>
            </div>
            <div class="field-row">
                <?php echo $form->labelEx($model,'e_dbpwd'); ?>
                <?php echo $form->passwordField($model,'e_dbpwd'); ?>
                <?php echo $form->error($model,'e_dbpwd'); ?>
            </div>
            <div class="field-row">
                <?php echo $form->labelEx($model,'re_password'); ?>
                <?php echo $form->passwordField($model,'re_password'); ?>
                <?php echo $form->error($model,'re_password'); ?>
            </div>
            <div class="field-row">
                <?php echo $form->labelEx($model,'domain'); ?>
                <div class="site-domain-partner">
                    <span>http://</span>
                    <?php echo $form->textField($model,'domain',array('size'=>30,'class'=>'input-small')); ?>
                    <span>.launchproject.com</span>
                </div>
                <?php echo $form->error($model,'domain'); ?>
            </div>
            <div class="field-row">
                <?php echo $form->labelEx($model,'website'); ?>
                <?php echo $form->textField($model,'website'); ?>
                <?php echo $form->error($model,'website'); ?>
            </div>
            <div class="user-field-captcha">
                <?php
                //echo $form->labelEx($model, 'validation');
                $this->widget('application.extensions.recaptcha.EReCaptcha',
                    array('attribute'=>'validateCaptSite',
                        'theme'=>'red', 'language'=>'en_US',
                        'publicKey'=>Yii::app()->params['recaptcha']['publicKey']));
                echo $form->error($model, 'validateCaptSite');
                ?>
            </div>

            <div class="user-field-terms">
                <?php echo $form->checkBox($model,'user_terms'); ?>
                <?php echo $form->labelEx($model,'user_terms'); ?>
                <?php echo $form->error($model,'user_terms'); ?>
            </div>
            <p class="user-read-terms">
                <?php echo Yii::t('_yii','By clicking on \'I Agree\' you are confirming that you have read, understood, and accept the Terms of Service Privacy Policy');?>
            </p>

            <div class="form-actions">
                <?php echo CHtml::submitButton('Sign Up', array('id'=>'whitelableId', 'class'=>'btn btn-primary')); ?>
            </div>

            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>