<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'id' => 'launch.com',
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Launch',
    'theme'=>'launch',
    'language' => 'en',
    'sourceLanguage'=>'en',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.widgets.*',
        'application.components.helpers.*',
        'application.components.helpers.utils.*',
        'ext.phpthumb.*',
	),
	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => '1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters' => array('127.0.0.1', '::1'),
		),

	),
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.AppStartup'
        ),
    ),

	// application components
	'components'=>array(
        'user' => array(
            'class' => 'application.components.WebUser',
            'allowAutoLogin' => true,
        ),
        /*'session' => array(
            'class' => 'LaunchHttpSession',
            'sessionName' => 'launchSession',
            'savePath' => 'protected/runtime/sessions',
            'cookieParams' => array(
                'path' => ''
            ),
        ),*/
        /*'messages' => array(
            'class' => 'application.components.messages.CXmlMessageSource',
            'cachingDuration' => 24 * 60 * 60, // 24h
        ),*/
		// uncomment the following to enable URLs in path-format
        //email
        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.mailer',
            'pathLayouts' => 'application.views.mailer.layouts'
        ),
		'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
                '<language:(en|vi|de)>/' => 'site/index',
                '<language:(en|vi|de)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<language:(en|vi|de)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<language:(en|vi|de)>/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '<language:(en|vi|de)>/login' => 'user/login',
                '<language:(en|vi|de)>/logout' => 'site/logout',
                '/login' => 'user/login',
                '/logout' => 'site/logout',
                '<language:(en|vi|de)>/<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
                '<language:(en|vi|de)>/<controller:\w+>'=>'<controller>/index',
			),
		),

		/* 'db'=>array(
		  'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		  ), */
		// uncomment the following to use a MySQL database
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=launch_yii',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'hd',
			'charset' => 'utf8',
		),
		'errorHandler' => array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			// uncomment the following to show log messages on web pages
			/*
			  array(
			  'class'=>'CWebLogRoute',
			  ),
			 */
			),
		),
	),
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => array(
		// this is used in contact page
		'adminEmail' => 'webmaster@gmail.com',
		'adminName' => 'Launch Admin',
		'languages' => array('vi' => 'Vietnamese', 'en' => 'English', 'de' => 'Deutsch'),
        'uploadPath'=>dirname(__FILE__).'/../../images/files',
        'uploadUrl'=>'/images/files',
		'oauth' => array(
			"base_url" => "http://" . $_SERVER['SERVER_NAME'] . "/oauth/index",
			"providers" => array(
				"Facebook" => array(
					"enabled" => true,
					"keys" => array("id" => "706255712765745", "secret" => "2e8bfff249cc223cc22392885c332501"),
					"scope" => "email",
					"display" => "popup"
				),
				"Twitter" => array(
					"enabled" => true,
					"keys" => array("key" => "IngXJ4cF2QiXWQtMUzJLEsdT9", "secret" => "SqUbkhuMjpb00i2IYu0mkFI8m7E13kf48uD3ForjZaZpRMn277"),
				),
				"Google" => array(
					"enabled" => true,
					"keys" => array("id" => "961151416683-tavbgdg4cs4pqbdhj61a3dt1sjrbtj11.apps.googleusercontent.com", "secret" => "lks2C_5ynPLkAVS9VCiGQDsr"),
				),
			),
			"debug_mode" => false,
			// to enable logging, set 'debug_mode' to true, then provide here a path of a writable file 
			"debug_file" => "",
		),
		'recaptcha' => array(
            'publicKey' => '6Le66PQSAAAAANp6_SqnzCLRd7knmKjxob1kthFk',
            'privateKey' => '6Le66PQSAAAAAAa4hwS7AzzeldsqrKsIElY3Sj7g',
        ),
		'expried_token' => 1, //Token reset password, unit : Day
		'smtp' => array(
            "Host"=>'smtp.gmail.com',
            "Username"=>'dapworld.test@gmail.com',
            "Password"=>'dapworld1234567dapworld',
            "From"=>'vnlabcode@gmail.com',
            "SMTPDebug" => 0,
            "SMTPAuth"=> true,
            "SMTPSecure"=>'ssl',
            "Port"=>465,
            "FromName"=>'launch.com',
        ),
	),
);
