<?php

class EventController extends Controller
{
	public function actionIndex()
	{
        $loadFile = new LoaderUtil();
        $model=new VEvent();
        $files = array(
            'css/liveevent.css',
            'js/liveevent.js',
        );
        $loadFile->load('theme',$files);

        if(ST_CUR_DOMAIN == ST_DOMAIN){
            $table = 'tbl_live_event';
        }else{
            $table = 'vw_live_event';
        }
        $events = Yii::app()->db->createCommand()
	            ->select('title_event, description_event')
	            ->from($table)
                ->queryAll();
        $data = array();
        $data['events'] = $events;
		$this->render('index',$data);
	}

}