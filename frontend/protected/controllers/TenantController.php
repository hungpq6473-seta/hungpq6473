<?php

class TenantController extends Controller
{
    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionCreate(){
        $loadFile = new LoaderUtil();
        $loadFile->load('root', array('css/siteAccount.css','js/siteAccount.js'));
        $initTUser = new TUser();
        $model = new TTenant();
        $model->scenario = 'registerwcaptcha';
        if(isset($_POST['TTenant']))
        {
            $params = $_POST['TTenant'];
            $model->attributes=$params;
            $model->created = time();
            if($model->validate()){
                $model->scenario = NULL;
                $model->e_dbpwd = base64_encode(base64_encode($params['e_dbpwd'].'-launch'));
                $model->re_password = base64_encode(base64_encode($params['re_password'].'-launch'));
                $model->domain=$params['domain'].'.'.ST_DOMAIN;
                if($model->save())
                    $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }
}