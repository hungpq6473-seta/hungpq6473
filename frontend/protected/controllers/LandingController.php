<?php

class LandingController extends Controller {
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
	
	public function actionIndex(){
		$this->layout = '//layouts/landing';
		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/landingContent.css'));
		$this->render('index');
	}
}