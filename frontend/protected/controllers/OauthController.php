<?php

class OauthController extends Controller {

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        global $tenant;
        $provider = strtolower(Yii::app()->request->getQuery('provider'));
        // for now, only accept login from facebook and twitter, google
        if (!in_array($provider, array('facebook', 'twitter', 'google'))) {
            $provider = 'facebook';
        }
		//From main site = true if login from launch, and = false if login from partner site
		$fromMainSite = true;
        //If google we get cookie sent from subdomain
        if ($provider == 'google' && isset($_COOKIE['tenant'])) {
            $tenant = json_decode($_COOKIE['tenant'], true);
			$fromMainSite = false;
        }
        try {
            $oauthProvider = Oauth::getInstance();
            if (isset($_GET['provider'])) { //First time
                $oauthProvider->authenticateSocial($provider);
                $userOauthProfile = $oauthProvider->userProfile;
            } else { //Redirect to base Url
                $oauthProvider->processEndpoint();
            }
        } catch (Exception $e) {
            // Hiding technical error message
            // Seem raise error code 5, close the poup immediately
            echo '<script>alert("Service die !!!");window.close();</script>';
            exit;
        }

        # 1 - check if user already have authenticated using this provider before
        $userOauth = TUserOauth::model()->findByAttributes(
                array(
                    'provider' => $provider,
                    'identifier' => $userOauthProfile->identifier,
                    'tenant_id' => $tenant['tenant_id'],
                    'tenant_dbu' => $tenant['dbu']
                )
        );
        # 2 - if authentication exists in the database, then we set the user as connected and redirect him to home page
        if ( is_object($userOauth) && $userOauth->identifier && $userOauth->user_id) {
            $userModel = TUser::model()->findByPk($userOauth->user_id);
            if ( is_object($userModel) && $userModel->status == 1) {
                //if google, we dont login, only send info login via cookies, after redirect to subdomain, we will login after
                if ($provider == 'google' && !$fromMainSite) {
                    $arrInfo = array(
                        'identifier' => $userOauth->identifier,
                        'username' => $userModel->email
                    );
                    //ST_CUR_DOMAIN = launch.com because popup open always launch.com
                    $expire = time() + 60 * 30;
                    setcookie("send_back_login", json_encode($arrInfo), $expire, "/", ST_CUR_DOMAIN, 0, true);
                    $result = array(
                        'error' => 0,
                        'redirect' => $oauthProvider->getRedirect($tenant['tenant_id'])
                    );
                } else {
                    //Make user login to our system
                    if ($oauthProvider->makeLogin($userOauth->identifier, $userModel->email)) {
                        $result = array(
                            'error' => 0,
                            'redirect' => $oauthProvider->getRedirect()
                        );
                    } else {
                        $result = array(
                            'error' => 1,
                            'message' => 'Can not login.'
                        );
                    }
                }
            } else {
                $result = array(
                    'error' => 1,
                    'message' => 'Your account has been suspended or deleted.'
                );
            }
        } else {
            # 3 - else, here lets check if the user email we got from the provider already exists in our database ( for this example the email is UNIQUE for each user )
            // if authentication does not exist, but the email address returned  by the provider does exist in database,
            // then we tell the user that the email  is already in use
            // but, its up to you if you want to associate the authentication with the user having the adresse email in the database
            // Make temp username
            $userOauthProfile->username = $oauthProvider->generateUsername($userOauthProfile->firstName, $userOauthProfile->lastName);

            //Note Twitter API doesnot provide email
            if ($userOauthProfile->email) {
                $user = TUser::model()->findByAttributes(array('email' => $userOauthProfile->email));

                if ( is_object($user) && $user->id) {
                    // The user's email of $provider account has been registered.
                    $userOauthProfile->email = null;
                }
            }

            // Now we create a temp email
            if (!$userOauthProfile->email) {
                switch ($provider) {
                    case 'facebook':
                        $userOauthProfile->email = "{$userOauthProfile->username}@facebook.com";
                        break;
                    case 'twitter':
                        $userOauthProfile->email = "{$userOauthProfile->username}@twitter.com";
                        break;
                    case 'google':
                        $userOauthProfile->email = "{$userOauthProfile->username}@google.com";
                        break;
                }
            }

            # 4 - if authentication does not exist and email is not in use, then we create a new user
            // 4.1 - create new user
            $newUser = new TUser();
            //for now, we use email make username
            //$newUser->username = $userOauthProfile->username;
            $newUser->username = $userOauthProfile->email;
            $newUser->full_name = $userOauthProfile->displayName;
            //general new pass for social user, use for action makeLogin after
            $newUser->password = $newUser->hashPassword($userOauthProfile->identifier, $newUser->username);
            $newUser->email = $userOauthProfile->email;
            $newUser->status = 1;
            $newUser->created = time();
            $newUser->tenant_owner = 0;
            $newUser->role_id = 3;
            $newUser->tenant_id = $tenant['tenant_id'];
            $newUser->tenant_dbu = $tenant['dbu'];


            //If use $newUser->save() -> doesnt work because capcha verified in rules()
            if ($newUser->insert()) {
                // 4.2 - creat a new authentication for him
                $newUserOauth = new TUserOauth();
                $newUserOauth->user_id = $newUser->id;
                $newUserOauth->provider = $provider;
                $newUserOauth->identifier = $userOauthProfile->identifier;
                $newUserOauth->profile_cache = json_encode($userOauthProfile);
                $newUserOauth->session_data = $oauthProvider->getSessionData();
                $newUserOauth->tenant_id = $tenant['tenant_id'];
                $newUserOauth->tenant_dbu = $tenant['dbu'];
                $newUserOauth->save();
                //if google, we dont login, only send info login via cookies, after redirect to subdomain, we will login after
                if ($provider == 'google' && !$fromMainSite) {
                    $arrInfo = array(
                        'identifier' => $newUserOauth->identifier,
                        'username' => $newUser->username
                    );
                    //ST_CUR_DOMAIN = launch.com because popup open always launch.com
                    $expire = time() + 60 * 30;
                    setcookie("send_back_login", json_encode($arrInfo), $expire, "/", ST_CUR_DOMAIN, 0, true);
                    $result = array(
                        'error' => 0,
                        'redirect' => $oauthProvider->getRedirect($tenant['tenant_id'])
                    );
                } else {
                    //Make user login to our system
                    if ($oauthProvider->makeLogin($newUserOauth->identifier, $newUser->username)) {
                        $result = array(
                            'error' => 0,
                            'redirect' => $oauthProvider->getRedirect()
                        );
                    } else {
                        $result = array(
                            'error' => 1,
                            'message' => 'Can not login.'
                        );
                    }
                }
            } else {
                $result = array(
                    'error' => 1,
                    'message' => 'Error when save user.'
                );
            }
        }
        //Delete cookies sent from actionLogin
        if ($result['error'] == 0 && isset($_COOKIE['tenant'])) {
            $expire = time() - 60 * 30;
            setcookie("tenant", json_encode($tenant), $expire, "/", ST_DOMAIN, 0, true);
        }
        $this->render('index', array('result' => $result));
    }
}
