<?php

class OffersController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionSpecial(){
        $page = (isset($_GET['page']) ? $_GET['page'] : 1);
        $loadFile = new LoaderUtil();
        $model    = new TOffers();
        $files    = array(
            'css/offers.css',
            'js/offers.js',
        );
        $loadFile->load('root', $files);
        $start = 5*($page-1);
        $offers = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tbl_offers')
            ->order('created DESC')
            ->limit(5, $start)
            ->queryAll();

        $data          = array();
        $data['offers'] = $offers;
        $this->render('special',$data);
    }
    public function actionLoadSpecial() {
        $page = (isset($_GET['page']) ? $_GET['page'] : 1);
        $start = 5*($page-1);
        $offers = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tbl_offers')
            ->order('created DESC')
            ->limit(5, $start)
            ->queryAll();
        foreach( $offers as $key=>$item ){
            $this->renderPartial('_item');
        }
        die;
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}