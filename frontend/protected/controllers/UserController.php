<?php

class UserController extends Controller {
    private $_model;
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }


    /**
    * Login Form
    **/
    public function actionLogin() {
        if (Yii::app()->user->isGuest) {
            global $tenant;
            $loadFile = new LoaderUtil();
            $model = new LoginForm;
            $model->scenario = 'loginwcaptcha';
            $loadFile->load('root', array('css/loginAccount.css','js/loginAccount.js'));
            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
            // collect user input data
            if (isset($_POST['LoginForm'])) {

                $model->attributes = $_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid

                if ($model->validate() && $model->login()) {
                    $model->scenario = NULL;
                    $domain = TTenant::model()->find(array(
                        'select' => 'domain',
                        'condition' => 'id=:tenant_id',
                        'params' => array(':tenant_id' => Yii::app()->user->tenant_id),
                    ));
                    $this->redirect('http://' . $domain->domain);
                }
            }

            //if subdomain -> send Tenant info to Main domain, use for redirect after authen
            if (ST_CUR_DOMAIN != ST_DOMAIN) {
                //$domain always = launch.com
                $expire = time() + 60 * 30;
                setcookie("tenant", json_encode($tenant), $expire, "/", ST_DOMAIN, 0, true);
            }

            $data = array(
                'model' => $model,
                'domain' => ST_DOMAIN
            );
            $this->render('login', $data);
        }else
            $this->redirect(Yii::app()->homeUrl);

    }

    /**
     * Action create New Account
     **/
    public function actionCreate() {
        global $tenant;
        $loadFile = new LoaderUtil();
        $loadFile->load('root', array('css/userAccount.css','js/userAccount.js'));
        $model = new TUser();
        $model->scenario = 'registerwcaptcha';
        if (isset($_POST['TUser'])) {
            $model->attributes = $_POST['TUser'];
            $model->created = time();
            $model->tenant_id = $tenant['tenant_id'];
            $model->tenant_dbu = $tenant['dbu'];
            if($model->validate()){
                $model->scenario = NULL;
                $model->password = $model->hashPassword($_POST['TUser']['password'], $_POST['TUser']['username']);
                $model->re_password = $model->hashPassword($_POST['TUser']['re_password'], $_POST['TUser']['username']);
                $model->auth_key = BaseSecurity::generateRandomKey();
                if ($model->save()){
                    $activation_url = $this->createAbsoluteUrl('/user/activate', array('key' => $model->auth_key, 'email' => $_POST['TUser']['email']));
                    $to = $_POST['TUser']['email'];
                    $fromName = 'Launch';
                    $replyemail = NULL;
                    $subject = 'Active Account';
                    $params = array(
                        'full_name' => $_POST['TUser']['full_name'], 'email'=>$_POST['TUser']['email'], 'activation_url' => $activation_url
                    );
                    $view = 'register';
                    $layout = 'main1';
                    $sendmail = BaseEmail::sendHtmlEmail($to,$fromName,$replyemail,$subject,$params,$view,$layout);
                    if($sendmail){
                        $successmsg = Yii::t('_yii', 'An active link has been sent to your email. Please check your email to activate your account');
                        Yii::app()->user->setFlash('success', $successmsg);
                        $this->redirect('/login');
                    }else{
                        return false;
                    }

                }else
                    $model->password = $_POST['TUser']['password'];
            }

        }
		
        //if subdomain -> send Tenant info to Main domain, use for redirect after authen
        if (ST_CUR_DOMAIN != ST_DOMAIN) {
            //$domain always = launch.com
            $expire = time() + 60 * 30;
            setcookie("tenant", json_encode($tenant), $expire, "/", ST_DOMAIN, 0, true);
        }

        $this->render('create', array(
            'model' => $model,
			'domain' => ST_DOMAIN
        ));
    }

    /**
    * Action Active Account after register.
    **/
    public function actionActivate($key, $email)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 'auth_key=:activation_key AND email=:email';
        $criteria->params = array(':activation_key' => $key, ':email' => $email);
        $user = TUser::model()->find($criteria);
        if ($user) {
            $user->auth_key = NULL;
            $user->status = TUser::STATUS_ACTIVE;
            $user->save(false); //user has already been validated when saved for the forst time.
            $successmsg = Yii::t('registration', 'Welcome! Your account has been activated. Now you can log in.');
            Yii::app()->user->setFlash('success', '<div>Hi! '.$user->username.'</div>' . $successmsg);
            $this->redirect('/login');
        } else {
            $errormsg = Yii::t('_yii', ' Error.Your account could not be activated,please repeat the registration process.');
            $criteria = new CDbCriteria;
            $criteria->condition = ' email=:email';
            $criteria->params = array(':email' => $email);
            $user = TUser::model()->find($criteria);
            $user->delete();
            Yii::app()->user->setFlash('danger', $errormsg);
            $this->redirect('/user/create');
        }
    }

    // Load ajax check email exits;
    public function actionCheckobj(){
        $result = 1;
        $objVal = isset($_POST['value'])?$_POST['value']:null;
        if($objVal){
            if (strpos($objVal, '@') !== false) {
                $cond = array('email' => $objVal);
            }else{
                $cond = array('username' => $objVal);
            }

            $users = TUser::model()->findByAttributes($cond);
            if($users){
                $result = 0;
            }
        }
        echo $result;exit;
    }

    // methods isUserTenantAdmin() and isUserAppStaff() are in protected/components/WebUser.php;
    // there are different methods for each user_type
    // use the controller’s accessRules() method to control access
    // this way you only need one controller for both tables and MySQL views
    public function loadModel($id) {
        $model = (Yii::app()->user->isUserTenantAdmin()) ? VUser::model()->findByPk($id) : TUser::model()->findByPk($id);
        if ($model === NULL) {
            throw new CHttpException(404, 'The requested user does not exist.');
        }
        return $model;
    }

    /**
    * Action change account
    * Change infomation account with password.
    **/
    public function actionAccount(){
        $this->layout = '//layouts/user';
        $users = $this->loadUser();
        $model = new TUser();
        $loadFile = new LoaderUtil();
        $loadFile->load('root',array('css/configAccount.css','js/configAccount.js'));

        if(isset($_POST['TUser'])){
            $params = $_POST['TUser'];
            $model->attributes = $_POST['TUser'];
            $username = isset($users->username)?$users->username:'';
            $password = $model->hashPassword($params['password'], $username);

            $userId = $users->id;
            $user = TUser::model()->findByPk($userId);
            $user->password = $password;
            if($user->update())
                Yii::app()->user->setFlash('success', Yii::t('_yii','Your password change successfully!'));
            //$this->redirect('/');

        }
        $this->render('account', array(
            'model' => $model,
        ));
    }

    /**
    * Action config Profile user.
     * Change Avatar, info...
    **/
    public function actionProfile(){
        global $user;
        $this->layout = '//layouts/user';
        $users = $this->loadUser();
        $model = new UserProfile();
        $loadFile = new LoaderUtil();
        $loadFile->load('root',array('css/configAccount.css','css/dropdown.css','js/STDrop.js','js/configAccount.js','js/STMaxlength.js'));
        $companyType = array(
            1=>Yii::t('_yii','Business and Industrial Markets'),
            2=>Yii::t('_yii','Business and Industrial Markets'),
            3=>Yii::t('_yii','Business and Industrial Markets'),
        );

        if(isset( $_POST['UserProfile'])  ){
            $params = $_POST['UserProfile'];
            $model->attributes = $_POST['UserProfile'];
            //$avatar=CUploadedFile::getInstance($model,'avatar');
            if($model->validate())
            {
                $userId = $users->id;
                $user = TUser::model()->findByPk($userId);
                $user->full_name = $params['full_name'];
                $user->company = $params['company'];
                $user->company_type = $params['company_type'];
                $user->bio_user = $params['bio_user'];
                $user->keep_private = $params['keep_private'];
                if($params['avatar']) $user->avatar = $params['avatar'];
                if($user->update()){
                    Yii::app()->user->setFlash('success',Yii::t('_yii','Your profile changed successfully!'));
                }

            }
        }
        $objThumb = new ThumbBases();
        if($user['avatar']){
            $pathAvatar = 'avatar/'.$user['avatar'];
        }else{
            $pathAvatar = 'avatar/default_profile_picture.jpg';
        }
        $avatar = $objThumb->Thumbs($pathAvatar,125, 125);
        $data = array(
            'model'=>$model,
            'comType'=>$companyType,
            'avatar'=>$avatar,
        );
        $this->render('profile', $data);
    }

    public function actionUpload(){
        global $user;
        $params = $_FILES;
        $uploadPath = Yii::app()->params['uploadPath'];

        if (!file_exists($uploadPath.'/avatar/' . $user['id'])) {
            mkdir($uploadPath.'/avatar/' . $user['id']);
        }
        $allowedExts = array("png", "PNG", "jpg", "JPG", 'gif',"GIF","jpeg","JPEG");
        $extension = explode(".", $params["uploadphoto"]["name"]);
        $extension = $extension[count($extension) - 1];
        if (($params["uploadphoto"]["size"] <= 20*1024*1024) && in_array($extension, $allowedExts)){
            if ($params["uploadphoto"]["error"] > 0) {
                $url = '';
            } else {
                $name = time() . '-' . rand(1000, 9999) . '-' . date('Y-m-d-H-i-s') . '.' . $extension;
                $url = $user['id'] . '/' . $name;
                $pathBase = $uploadPath . '/avatar/'.$user['id'];
                $path = $uploadPath . '/avatar/' . $url;
                if (move_uploaded_file($params["uploadphoto"]["tmp_name"], $path)) {
                    @chmod($path, 0777);
                    @chmod($pathBase, 0777);
                    $url = $url;
                } else {
                    $url = '';
                }
            }
        } else {
            $url = '';
        }

        if($url){
            $thumbs = new ThumbBases();
            $pathUrl = 'avatar/'.$url;
            $file = $thumbs->Thumbs($pathUrl,125,125);
            $files = array(
                'file'=>$file,
                'name'=>$url
            );
        }
        echo json_encode($files);exit;
    }

    /**
    * Check permision user loged or guest
     **/
    public function loadUser()
    {
        if($this->_model===null)
        {
            if(Yii::app()->user->id)
                $this->_model=Yii::app()->user;
            if($this->_model===null)
                $this->redirect(array('/login'));
        }
        return $this->_model;
    }
	
	public function actionForgot() {
		global $tenant;
		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/forgotPw.css'));
		$model = new ForgotPwForm;
		$model->scenario = 'resetwcaptcha';
		if (isset($_POST['ForgotPwForm'])) {
			$model->setAttributes($_POST['ForgotPwForm']);
			if ($model->validate()) {
				$cond = array(
					'tenant_id' => $tenant['tenant_id'],
					'tenant_dbu' => $tenant['dbu']
				);
				if ($model->email) {
					$cond = array_merge($cond, array('email' => $model->email));
				}
				if ($model->username) {
					$cond = array_merge($cond, array('username' => $model->username));
				}
				//Find user reset Pw
				$user = TUser::model()->findByAttributes($cond);
				//Find existed token of user in DB
				$userKey = TUserKey::model()->findByAttributes(array('user_id' => $user->id));
				if (!is_object($userKey)) {//If existed -> override, if not ->add new
					$userKey = new TUserKey();
				}
				$token = $userKey->generateToken($user->email);
				$userKey->user_id = $user->id;
				$userKey->key = $token;
				$userKey->created = time();
				$userKey->consume_time = Yii::app()->params['expried_token'];
				//$userKey->expire_time = $userKey->created + $userKey->consume_time * 24 * 60 * 60;
				$userKey->expire_time = $userKey->created + 2 * 60;
				$userKey->type = 1;
				if ($userKey->save()) {
					//Send mail to user
					$activation_url = $this->createAbsoluteUrl('/user/resetPassword', array('token' => $token, 'tmp' => md5($user->email)));
					$activation_url = CHtml::link('Click Here!', $activation_url);
                    $fromName = 'Launch Admin';
                    $replyemail = NULL;
                    $subject = Yii::t('_yii', 'Confirm reset password');
                    $params = array(
                        'message' => 'Click the link to reset your password', 'activation_url' => $activation_url
                    );
                    $view = 'forgot';
                    $layout = 'main1';
                    BaseEmail::sendHtmlEmail($user->email,$fromName,$replyemail,$subject,$params,$view,$layout);
					
                    Yii::app()->user->setState('message', Yii::t('_yii', 'An email confirmed was sent to you. Please check your email for more detail.'));
                    $this->redirect(array('user/notice'));
				}
			}
		}
		$this->render('forgot', array('model' => $model));
	}
	
	public function actionResetPassword(){
		$token = Yii::app()->request->getParam('token');
		$md5Email = Yii::app()->request->getParam('tmp');
        $valid = FALSE;
		//Find existed token
		$userKey = TUserKey::model()->findByAttributes( array('key'=>$token) );
		if(is_object($userKey) && $userKey->user_id){//If existed
			//Find existed user
			$user = TUser::model()->findByPk($userKey->user_id);
			//If existed user and matching email with param tmp in link
			if(is_object($user) && $user->email && md5($user->email) == $md5Email ){
                //Check expired time
                if( time() <= $userKey->expire_time ){
                    $valid = TRUE;
                } else {
                    $mes = Yii::t('_yii', 'Token is expired.');
                }
			} else { // if not
				$mes = Yii::t('_yii', 'Invalid Token.');
			}
		} else { //Token does not valid.
			$mes = Yii::t('_yii', 'Invalid Token.');
		}
        
        if(!$valid){
            Yii::app()->user->setState('message', $mes);
            $this->redirect(array('user/notice'));
        }
		
		global $tenant;
		$loadFile = new LoaderUtil();
		$loadFile->load('root', array('css/resetPw.css'));
		$model = new ResetPwForm;
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'reset-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
		if (isset($_POST['ResetPwForm'])) {
			$model->setAttributes($_POST['ResetPwForm']);
			if ($model->validate()) {
				//Delete token for user
                $userKey->delete();
                //Update pw and Auto login
                $user->password = $user->hashPassword($model->password, $user->username);
                $user->update();
                $modelLogin = new LoginForm;
                $modelLogin->username = $user->username;
                $modelLogin->password = $model->password;
                if ($modelLogin->login()) {
                    $domain = TTenant::model()->find(array(
                        'select' => 'domain',
                        'condition' => 'id=:tenant_id',
                        'params' => array(':tenant_id' => $tenant['tenant_id']),
                    ));
                    $this->redirect('http://' . $domain->domain);
                } else {
                    $this->redirect(array('user/login'));
                }
			}
		}
		$this->render('reset', array('model' => $model));
	}

    public function actionPrivate(){
        global $user;
        $status = isset($_POST['status'])?$_POST['status']:NULL;
        $result = 0;
        if($status != NULL){
            $userId = $user['id'];
            $user = TUser::model()->findByPk($userId);
            $user->keep_private = $status;
            if($user->update()){
                $result = 1;
            }
        }
        echo $result;exit;
    }

    //Action show message after forgot and reset Pw
    public function actionNotice(){
        $message =  Yii::app()->user->getState('message');
        $this->render('notice', array('message' => $message));
    }

}
