<?php

class BookstoreController extends Controller {
	public function actionIndex() {
		$loadFile = new LoaderUtil();
		$model    = new TBookstore();
		$files    = array(
			'css/bookstore.css',
			'js/bookstore.js',
		);
		$loadFile->load('root', $files);
		$data          = array();
		$data['model'] = $model;
		$this->render('index', $data);
	}

	public function actionLoadBook() {
		$this->renderPartial('_item');
		die;
	}

	public function actionDetail() {
		$loadFile = new LoaderUtil();
		$loadSeo = new SEOUtil();
		$model    = new TBookstore();
		$files    = array(
			'css/bookstore.css',
			'js/bookstore.js',
            'js/SocialSharer.js'
		);
		$loadFile->load('root', $files);
        Yii::app()->clientScript->registerScriptFile('http://w.sharethis.com/button/buttons.js', CClientScript::POS_END);
        $desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book";
        $image = "http://launch.com/images/files/book/Book_Item_Details.png";
        $link = 'http://launch.com/bookstore/detail';
        $title='launch!: the critical 90 days from idea to market';
        $type='article';
        $loadSeo->metaSocial($link,$title,$desc,$image,$type);
		$data          = array();
		$data['model'] = $model;
		$this->render('detail', $data);
	}
	public function actions() {
		// return external action classes, e.g.:
		return array(
			'action1'       => 'path.to.ActionClass',
			'action2'       => array(
				'class'        => 'path.to.AnotherActionClass',
				'propertyName' => 'propertyValue',
			),
		);
	}

}