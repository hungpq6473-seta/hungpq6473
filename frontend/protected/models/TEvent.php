<?php

/**
 * This is the model class for table "tbl_live_event".
 *
 * The followings are the available columns in table 'tbl_live_event':
 * @property integer $eid
 * @property string $title_event
 * @property string $description_event
 * @property integer $create_by
 * @property string $location
 * @property integer $tenant_id
 * @property string $tenant_dbu
 * @property integer $status
 * @property integer $created
 */
class TEvent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_live_event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('create_by, tenant_id, status, created', 'numerical', 'integerOnly'=>true),
			array('title_event', 'length', 'max'=>355),
			array('location, tenant_dbu', 'length', 'max'=>255),
			array('description_event', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('eid, title_event, description_event, create_by, location, tenant_id, tenant_dbu, status, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'eid' => 'Eid',
			'title_event' => 'Title Event',
			'description_event' => 'Description Event',
			'create_by' => 'Create By',
			'location' => 'Location',
			'tenant_id' => 'Tenant',
			'tenant_dbu' => 'Tenant Dbu',
			'status' => 'Status',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('eid',$this->eid);
		$criteria->compare('title_event',$this->title_event,true);
		$criteria->compare('description_event',$this->description_event,true);
		$criteria->compare('create_by',$this->create_by);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('tenant_id',$this->tenant_id);
		$criteria->compare('tenant_dbu',$this->tenant_dbu,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TEvent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
