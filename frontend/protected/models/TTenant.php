<?php

/**
 * This is the model class for table "tbl_tenant".
 *
 * The followings are the available columns in table 'tbl_tenant':
 * @property integer $id
 * @property string $domain
 * @property string $dbu
 * @property string $e_dbpwd
 * @property string $business_name
 * @property integer $tenant_user
 * @property integer $status
 * @property integer $created
 */
class TTenant extends CActiveRecord
{
    public $validateCaptSite;
    public $user_terms;
    public $re_password;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_tenant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('e_dbpwd, business_name, dbu, domain, full_name, user_terms', 'required'),
			array('status, created', 'numerical', 'integerOnly'=>true),
			array('domain, dbu, business_name, full_name, website', 'length', 'max'=>255),
			array('e_dbpwd, re_password', 'length', 'max'=>1024),
            array('e_dbpwd', 'compare', 'compareAttribute'=>'re_password'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, domain, dbu, e_dbpwd, business_name, status, created', 'safe', 'on'=>'search'),
            array('email', 'email','message'=>Yii::t('_yii',"The email isn't correct")),
            array('email', 'unique','message'=>Yii::t('_yii','Email already exists!')),
            array('validateCaptSite',
                'application.extensions.recaptcha.EReCaptchaValidator',
                'privateKey'=>Yii::app()->params['recaptcha']['privateKey'],
                'on' => 'registerwcaptcha'
            ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('_yii','ID'),
			'domain' => Yii::t('_yii','Site'),
			'dbu' => Yii::t('_yii','Username'),
			'e_dbpwd' => Yii::t('_yii','Password'),
			're_password' => Yii::t('_yii','Confirm Password'),
            'email' => Yii::t('_yii','Email Address'),
			'business_name' => Yii::t('_yii','Company<span>(optional)</span>'),
			'website' => Yii::t('_yii','Company Website'),
			'full_name' => Yii::t('_yii','Full name'),
			'status' => Yii::t('_yii','Status'),
			'created' => Yii::t('_yii','Created'),
            'validation'=>Yii::t('_yii', 'Enter both words separated by a space: '),
            'user_terms' => Yii::t('_yii','I agree to the <a href="#">terms and conditions</a>'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domain',$this->domain,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('dbu',$this->dbu,true);
		$criteria->compare('e_dbpwd',$this->e_dbpwd,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    public function beforeSave()
    {
        if ($this->isNewRecord) {
            Common::createMySQLUser($this->dbu,$this->e_dbpwd);
        }
        return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TTenant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
