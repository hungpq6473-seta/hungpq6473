<?php

/**
 * ResetPwForm class.
 * ResetPwForm is the data structure for keeping
 */
class ResetPwForm extends CFormModel {

	public $password;
	public $re_password;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			//array('password, re_password', 'required'),
			array('password, re_password', 'length', 'max'=>255, 'allowEmpty'=>false),
			array('password, re_password', 'length', 'min'=>6, 'allowEmpty'=>false),
            array('password', 'compare', 'compareAttribute'=>'re_password', 'message'=>Yii::t('_yii', 'The confirm password does not match.')),
			array('password, re_password', 'safe')
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'password' => Yii::t('_yii', 'New password'),
			're_password' => Yii::t('_yii', 'Confirm password'),
		);
	}
}
