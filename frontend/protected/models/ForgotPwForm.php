<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ForgotPwForm extends CFormModel {

	public $email;
	public $username;
	public $checkCapt;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules() {
		return array(
			array('email', 'email', 'message' => Yii::t('_yii', 'Invalid Email Address.')),
			array('email, username', 'blank'),
			array('email, username', 'matching'),
			array('email', 'existed'),
			array('username', 'existed'),
			array('checkCapt',
				'application.extensions.recaptcha.EReCaptchaValidator',
				'privateKey' => Yii::app()->params['recaptcha']['privateKey'],
				'on' => 'resetwcaptcha'
			),
			array('email, username', 'safe')
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels() {
		return array(
			'username' => Yii::t('_yii', 'Username'),
			'email' => Yii::t('_yii', 'Email Address'),
		);
	}
	
	public function matching($attribute, $params) {
		if (!$this->hasErrors() && $this->email && $this->username) {
			global $tenant;
			$cond = array(
				'tenant_id' => $tenant['tenant_id'],
				'tenant_dbu' => $tenant['dbu'],
				'email' => $this->email,
				'username' => $this->username
			);
			$user = TUser::model()->findByAttributes($cond);
			if (!$user) {
				$this->addError('email', Yii::t('_yii', 'Username and Email address do not match.'));
			}
		}
	}

	public function existed($attribute, $params) {
		if (!$this->hasErrors() && $this->$attribute) {
			global $tenant;
			$cond = array(
				'tenant_id' => $tenant['tenant_id'],
				'tenant_dbu' => $tenant['dbu']
			);
			switch ($attribute) {
				case 'email':
					$cond = array_merge ($cond, array($attribute => $this->email));
					$errMess = Yii::t('_yii', 'The email address does not exist.');
					break;
				default:
					$cond = array_merge ($cond, array($attribute => $this->username));
					$errMess = Yii::t('_yii', 'The username does not exist.');
					break;
			}
			$user = TUser::model()->findByAttributes($cond);
			if (!$user) {
				$this->addError($attribute, $errMess);
			}
		}
	}
	
	public function blank($attribute, $params) {
		if (!$this->hasErrors() && $this->email == '' && $this->username == '') {
			$this->addError('email', Yii::t('_yii', 'Input your username or email address.'));
		}
	}

}
