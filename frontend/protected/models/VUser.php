<?php

/**
 * This is the model class for table "vw_user".
 *
 * The followings are the available columns in table 'vw_user':
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property integer $tenant_id
 * @property string $tenant_dbu
 * @property integer $tenant_owner
 * @property string $password
 * @property integer $status
 * @property integer $created
 * @property integer $user_type
 */
class VUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vw_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, tenant_id, tenant_owner, status, created, user_type', 'numerical', 'integerOnly'=>true),
			array('username, full_name, tenant_dbu', 'length', 'max'=>255),
			array('password', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, full_name, tenant_id, tenant_dbu, tenant_owner, password, status, created, user_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        return array(
            'tenant' => array(self::HAS_MANY, 'VTenant', 'tenant_id'), // tenant MySQL view
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'full_name' => 'Full Name',
			'tenant_id' => 'Tenant',
			'tenant_dbu' => 'Tenant Dbu',
			'tenant_owner' => 'Tenant Owner',
			'password' => 'Password',
			'status' => 'Status',
			'created' => 'Created',
			'user_type' => 'User Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('tenant_id',$this->tenant_id);
		$criteria->compare('tenant_dbu',$this->tenant_dbu,true);
		$criteria->compare('tenant_owner',$this->tenant_owner);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created);
		$criteria->compare('user_type',$this->user_type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function afterSave() // required - database views don't expose this to the schema Yii can access
    {
        if ($this->getIsNewRecord()) {
            $this->id = Yii::app()->db->getLastInsertID(); // I understand this works because MySQL handles it by each individual net connection so there should be no multiuser contention or race conditions
        }
        return parent::afterSave();
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
