<?php

/**
 * This is the model class for table "vw_tenant".
 *
 * The followings are the available columns in table 'vw_tenant':
 * @property integer $id
 * @property string $domain
 * @property string $dbu
 * @property string $e_dbpwd
 * @property string $business_name
 * @property integer $tenant_user
 * @property integer $status
 * @property integer $created
 */
class VTenant extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vw_tenant';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('e_dbpwd', 'required'),
			array('id, tenant_user, status, created', 'numerical', 'integerOnly'=>true),
			array('domain, dbu, business_name', 'length', 'max'=>255),
			array('e_dbpwd', 'length', 'max'=>1024),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, domain, dbu, e_dbpwd, business_name, tenant_user, status, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'domain' => 'Domain',
			'dbu' => 'Dbu',
			'e_dbpwd' => 'E Dbpwd',
			'business_name' => 'Business Name',
			'tenant_user' => 'Tenant User',
			'status' => 'Status',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('domain',$this->domain,true);
		$criteria->compare('dbu',$this->dbu,true);
		$criteria->compare('e_dbpwd',$this->e_dbpwd,true);
		$criteria->compare('business_name',$this->business_name,true);
		$criteria->compare('tenant_user',$this->tenant_user);
		$criteria->compare('status',$this->status);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VTenant the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
