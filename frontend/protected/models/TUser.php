<?php

/**
 * This is the model class for table "tbl_user".
 *
 * The followings are the available columns in table 'tbl_user':
 * @property integer $id
 * @property string $username
 * @property string $full_name
 * @property string $email
 * @property string $new_email
 * @property string $tenant_dbu
 * @property integer $tenant_id
 * @property integer $tenant_owner
 * @property string $password
 * @property integer $role_id
 * @property integer $user_type
 * @property string $auth_key
 * @property integer $status
 * @property integer $created
 */
define('SALT_LENGTH', 10);
class TUser extends CActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BANNED = 2;        //TODO
    const STATUS_REMOVED = 3;    //TODO

    //keep these in one place.
    const PASSWORD_MIN = 1;
    const PASSWORD_MAX = 50;
    const USERNAME_MIN = 3;
    const USERNAME_MAX = 45;
    const EMAIL_MAX = 125;
    const EMAIL_MIN = 5;

    public $re_password;
    public $validateCapt;
    public $user_terms;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tenant_id, tenant_owner, role_id, account_type, user_type, status, created, company_type', 'numerical', 'integerOnly'=>true),
			array('username, password, full_name, email, new_email, tenant_dbu, auth_key, company', 'length', 'max'=>255),
            array('full_name', 'length', 'min'=>2),
            array('password', 'length', 'min'=>5),
            array('id,company, username, full_name, email, new_email, tenant_dbu, tenant_id, tenant_owner, password, role_id, account_type, user_type, auth_key, status, created', 'safe', 'on'=>'search'),
            //array('avatar', 'file', 'types'=>'jpg, gif, png, jpeg'),
            array('email', 'email','message'=>Yii::t('_yii',"The email isn't correct")),
            array('email', 'unique','message'=>Yii::t('_yii','Email already exists!')),
            array('username, password, email, full_name, re_password, account_type, user_terms','required'),
            array('password', 'compare', 'compareAttribute'=>'re_password'),
            array('validateCapt',
                    'application.extensions.recaptcha.EReCaptchaValidator',
                    'privateKey'=>Yii::app()->params['recaptcha']['privateKey'],
                    'on' => 'registerwcaptcha'
            ),
		);
	}

    public function relations()
    {
        return array(
            'tenant' => array(self::BELONGS_TO, 'TTenant', 'tenant_id'), // tenant table
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => Yii::t('_yii','Username'),
			'full_name' => Yii::t('_yii','Full Name'),
			'full_name_profile' => Yii::t('_yii','Public Name'),
			'company' => Yii::t('_yii','Company<span>(optional)</span>'),
			'company_name' => Yii::t('_yii','Company Name<span>(optional)</span>'),
			'email' => Yii::t('_yii','Email Address'),
			'company_type' => Yii::t('_yii','Company Type'),
			'bio_user' => Yii::t('_yii','Bio<span>(optional)</span>'),
			'new_email' => Yii::t('_yii','New Email'),
			'keep_private' => Yii::t('_yii','Keep Private'),
			'avatar' => Yii::t('_yii','Profile Photo'),
			'tenant_dbu' => Yii::t('_yii','Tenant Dbu'),
			'tenant_id' => Yii::t('_yii','Tenant'),
			'tenant_owner' => Yii::t('_yii','Tenant Owner'),
			'password' => Yii::t('_yii','Password'),
			're_password' => Yii::t('_yii','Confirm Password'),
			'role_id' => Yii::t('_yii','Role'),
			'user_type' => Yii::t('_yii','User Type'),
			'account_type' => Yii::t('_yii','Account Type'),
			'auth_key' => Yii::t('_yii','Auth Key'),
			'status' => Yii::t('_yii','Status'),
			'user_terms' => Yii::t('_yii','I agree to the <a href="#">terms and conditions</a>'),
			'created' => Yii::t('_yii','Created'),
            'validation'=>Yii::t('_yii', 'Enter both words separated by a space: '),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('new_email',$this->new_email,true);
		$criteria->compare('tenant_dbu',$this->tenant_dbu,true);
		$criteria->compare('tenant_id',$this->tenant_id);
		$criteria->compare('tenant_owner',$this->tenant_owner);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('role_id',$this->role_id);
		$criteria->compare('user_type',$this->user_type);
		$criteria->compare('account_type',$this->account_type);
		$criteria->compare('auth_key',$this->auth_key,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('company_type',$this->company_type);
		$criteria->compare('bio_user',$this->bio_user);
		$criteria->compare('avatar',$this->avatar);
		$criteria->compare('created',$this->created);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
    * @return boolean validate user
    */
	public function validatePassword($password, $username){
        return $this->hashPassword($password, $username);
	}
	/**
	 * @return hashed value
	 */
    public function hashPassword($phrase, $salt = null){

        $key = 'Gf;B&yXL|beJUf-K*PPiU{wf|@9K9j5?d+YW}?VAZOS%e2c -:11ii<}ZM?PO!96';
        if($salt == '')
            $salt = substr(hash('sha512', $key), 0, SALT_LENGTH);
        else
            $salt = substr($salt, 0, SALT_LENGTH);
        return hash('sha512', $salt . $key . $phrase);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
