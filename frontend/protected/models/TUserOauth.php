<?php

/**
 * This is the model class for table "tbl_user_oauth".
 *
 * The followings are the available columns in table 'tbl_user_oauth':
 * @property string $provider
 * @property string $identifier
 * @property integer $user_id
 * @property string $profile_cache
 * @property string $session_data
 * @property string $tenant_dbu
 * @property integer $tenant_id
 */
class TUserOauth extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user_oauth';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('identifier, user_id, tenant_id', 'required'),
			array('user_id, tenant_id', 'numerical', 'integerOnly'=>true),
			array('provider, identifier', 'length', 'max'=>45),
			array('tenant_dbu', 'length', 'max'=>255),
			array('profile_cache, session_data', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('provider, identifier, user_id, profile_cache, session_data, tenant_dbu, tenant_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'provider' => 'Provider',
			'identifier' => 'Identifier',
			'user_id' => 'User',
			'profile_cache' => 'Profile Cache',
			'session_data' => 'Session Data',
			'tenant_dbu' => 'Tenant Dbu',
			'tenant_id' => 'Tenant',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('provider',$this->provider,true);
		$criteria->compare('identifier',$this->identifier,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('profile_cache',$this->profile_cache,true);
		$criteria->compare('session_data',$this->session_data,true);
		$criteria->compare('tenant_dbu',$this->tenant_dbu,true);
		$criteria->compare('tenant_id',$this->tenant_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TUserOauth the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
