var BOOKSTORE = {
    init:function(){
        BOOKSTORE.ajaxLoadMore();
        BOOKSTORE.bookEvents();
    },

    /**
     * Ajax Load more data page;
     **/
    ajaxLoadMore:function(){
        jQuery('.book-load-more').click(function(e){
            var page= 1,result;
            e.preventDefault();
            AJAX.ajaxCallback = function(){
                if(AJAX.ajaxResult){
                    jQuery('.content-bookstore-page > ul').append(AJAX.ajaxResult);
                }
            };
            AJAX.ajaxData = {};
            AJAX.ajaxData.page = page;
            AJAX.ajaxAsync = false;
            AJAX.ajaxParseJSON = false;
            AJAX.ajaxUrl = PARAMS.baseUrl + '/bookstore/loadbook';
            AJAX.request();
            return result;
        });

    },

    bookEvents:function(){
        try {
            oHandler = jQuery("#sortbook").msDropDown({mainCSS:'dd2'}).data("dd");
            jQuery("#ver").html(jQuery.msDropDown.version);
        }catch(e) {

        }

        // Add Share Button
        try {
            stLight.options({publisher: "94bd2061-2ff9-4daa-bf37-2620997f8f7f", doNotHash: false, doNotCopy: false, hashAddressBar: false, popup: false});
            jQuery("#share a.share-social").jqSocialSharer();
        }catch(e) {

        }
    }
}

jQuery('document').ready(function(){
    BOOKSTORE.init();
});