/**
 * Created by Pham Quoc Hung on 6/9/14.
 */
var USERACCOUNT = {
    /**
     * Call all function in script;
     **/
    init:function(){
        USERACCOUNT.blurEvents();
        USERACCOUNT.changeTerms();
        USERACCOUNT.submitCreateAccount();
    },

    /**
     * Event Blur check validate field form;
     **/
    blurEvents:function(){
        jQuery('#TUser_email, #TUser_full_name, #TUser_company, #TUser_username, #TUser_password, #TUser_re_password').blur(function(){
            var objId, objCheck, _this=this,value, validate=true;
            value = jQuery.trim(jQuery(_this).val());
            objId = jQuery(_this).attr('id');
            if(objId == 'TUser_email'){
                objCheck = USERACCOUNT.validateEmail(value);
                if(!objCheck){
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    jQuery('label[for="TUser_email"] .invalid-email').remove();
                    jQuery('label[for="TUser_email"]').append('<span class="invalid-email">Invalid Email Address</span>');
                    validate = false;
                }else{
                    objCheck = USERACCOUNT.ajaxObjCheck(value);
                    if(!objCheck){
                        jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        jQuery('label[for="TUser_email"] .invalid-email').remove();
                        jQuery('label[for="TUser_email"]').append('<span class="invalid-email">Email already exists</span>');
                    }else{
                        jQuery('label[for="TUser_email"] .invalid-email').remove();
                        jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                    }

                }
            }else if(objId == 'TUser_full_name'){
                jQuery('label[for="TUser_full_name"] .invalid-email').remove();
                var patts = /[!@#$%^&*()\{\}<>~`?_\[\]\/:;|"1-9]/g;
                if( value.match(patts) || value.match(/\d/g) ){
                    jQuery('label[for="TUser_full_name"]').append('<span class="invalid-email">Invalid Full Name.</span>');
                    jQuery('#TUser_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                }else if(value.length == 0){
                    jQuery('label[for="TUser_full_name"]').append('<span class="invalid-email">Full Name cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else{
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }/*else if(objId == 'TUser_company'){
                jQuery('label[for="TUser_company"] .invalid-email').remove();
                if(value.length == 0){
                    jQuery('label[for="TUser_company"]').append('<span class="invalid-email">Company cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 2 ){
                    jQuery('label[for="TUser_company"]').append('<span class="invalid-email">The Company must from 2 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }*/else if(objId == 'TUser_username'){
                jQuery('label[for="TUser_username"] .invalid-email').remove();
                if(value.match(/\s/g)){
                    jQuery('label[for="TUser_username"]').append('<span class="invalid-email">Invalid User Name.</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if(value.length == 0){
                    jQuery('label[for="TUser_username"]').append('<span class="invalid-email">User name cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 4 || value.length > 24 ){
                    jQuery('label[for="TUser_username"]').append('<span class="invalid-email">The user name must be between 4 and 24 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    objCheck = USERACCOUNT.ajaxObjCheck(value);
                    if(!objCheck){
                        jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        jQuery('label[for="TUser_username"]').append('<span class="invalid-email">Username already exists</span>');
                    }else{
                        jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                    }
                }

            }else if(objId == 'TUser_password'){
                jQuery('label[for="TUser_password"] .invalid-email').remove();
                jQuery('label[for="TUser_password"] .invalid-strength').remove();
                if(value.length == 0){
                    jQuery('label[for="TUser_password"]').append('<span class="invalid-email">Password cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 6 || value.length > 18 ){
                    jQuery('label[for="TUser_password"]').append('<span class="invalid-email">Password must be between 6 and 18 characters</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    objCheck = USERACCOUNT.checkValidatePass(value);
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }

            }else if(objId == 'TUser_re_password'){
                jQuery('label[for="TUser_re_password"] .invalid-email').remove();
                if( value.length == 0 ){
                    jQuery('label[for="TUser_re_password"]').append('<span class="invalid-email">Confirm Password cannot be blank</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                }else if( value.length < 5 || value.length > 50){
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else{
                    var pass = jQuery('#TUser_password').val();
                    if(pass != value){
                        jQuery('label[for="TUser_re_password"]').append('<span class="invalid-email">The confirm password does not match.</span>');
                        jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                        validate = false;
                    }else
                        jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
                }

            }

            return validate;
        });
    },

    changeTerms:function(){
        jQuery('#TUser_user_terms').change(function(){
            var term = jQuery(this).is(':checked');
            if(!term){
                jQuery('.user-field-terms .invalid-email').remove();
                jQuery('.user-field-terms').append('<span class="invalid-email">Please check the box to indicate your acceptance.</span>');
            }else{
                jQuery('.user-field-terms .invalid-email').remove();
            }
        });
    },
    /**
     * Check validate email;
     **/
    validateEmail:function(email){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    /**
     * Check password
     ***/
    checkValidatePass:function(value){
        var i=0;
        var j=0;
        var ch='';
        var checkUpperCase = 0;
        var checkLowerCase = 0;
        var checkNumCase = 0;
        var checIntNum = 0;
        var checkfmCase = 0;
        var checkValue1 = '';
        var checkValue2 = '';
        var checkValue3 = '';
        var patt1=/[0-9]/g;
        var patt2=/[a-zA-Z]/g;
        var patt3=/[@*&^%$#!]/g;
        var checkInt = value.match(patt1) == null ? 0: 1;
        var checkChart1 = value.match(patt1) == null ? '': value.match(patt1);
        var checkChart2 = value.match(patt2) == null ? '': value.match(patt2);
        var checkChart3 = value.match(patt3) == null ? '': value.match(patt3);

        if(checkChart1 != ''){
            var myString = new String(checkChart1);
            var checkValue1 = myString.split(',').join('');
        }
        if(checkChart2 != ''){
            var myString = new String(checkChart2);
            var checkValue2 = myString.split(',').join('');
        }
        if(checkChart3 != ''){
            var myString = new String(checkChart3);
            var checkValue3 = myString.split(',').join('');
        }
        if(checkChart1 !=''){
            checkNumCase = 1;
        }
        if(checkChart3 !=''){
            checkfmCase = 1;
        }
        while (j < checkValue2.length){
            character2 = checkValue2.charAt(j);
            if (character2 == character2.toUpperCase()) {
                checkUpperCase = 1;
            }
            if ( character2 == character2.toLowerCase() ){
                checkLowerCase = 1;
            }
            j++;
        }
        jQuery('label[for="TUser_password"] .invalid-email').remove();
        jQuery('label[for="TUser_password"] .invalid-strength.weak').remove();
        jQuery('label[for="TUser_password"] .invalid-strength.moderate').remove();
        jQuery('label[for="TUser_password"] .invalid-strength.strong').remove();
        if( (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 1) || (checkUpperCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkfmCase == 1) || (checkNumCase == 1 && checkUpperCase == 1 && checkfmCase == 1) ){
            jQuery('label[for="TUser_password"]').append('<span class="invalid-strength strong">Password strength: <span>strong</span></span>');
        }
        if( ((checkfmCase != 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 1 && checkNumCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 1) || (checkUpperCase == 0 && checkLowerCase == 1 && checkNumCase == 1))) ||
            ((checkfmCase == 1) && ((checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkUpperCase == 1 && checkLowerCase == 0 && checkNumCase == 0))) ){
            jQuery('label[for="TUser_password"]').append('<span class="invalid-strength moderate">Password strength: <span>moderate</span></span>');
        }
        if( (checkfmCase != 1) && ((checkNumCase == 0 && checkLowerCase == 1 && checkUpperCase == 0) || (checkNumCase == 0 && checkLowerCase == 0 && checkUpperCase == 1) || (checkNumCase == 1 && checkLowerCase == 0 && checkUpperCase == 0)) ){
            jQuery('label[for="TUser_password"]').append('<span class="invalid-strength weak">Password strength: <span>weak</span></span>');
        }

    },

    /**
     * Ajax check Email or Username already exist;
     **/
    ajaxObjCheck:function(value){
        var result;
        AJAX.ajaxCallback = function(){
            result = parseInt(AJAX.ajaxResult);
        };
        AJAX.ajaxData = {};
        AJAX.ajaxData.value = value;
        AJAX.ajaxAsync = false;
        AJAX.ajaxParseJSON = false;
        AJAX.ajaxUrl = PARAMS.baseUrl + '/user/checkobj';
        AJAX.request();
        return result;
    },

    /**
     * Check data after click submit form.
     **/
    submitCreateAccount:function(){
        jQuery('#registerId').click(function(){
            var _this=this,fullname,email,pass,rePass,company,accountType,username,term,captcha,validate=true;
            fullname = jQuery('#TUser_full_name').val();
            email = jQuery('#TUser_email').val();
            pass = jQuery('#TUser_password').val();
            rePass = jQuery('#TUser_re_password').val();
            company = jQuery('#TUser_company').val();
            username = jQuery('#TUser_username').val();
            term = jQuery('#TUser_user_terms').is(':checked');
            captcha = jQuery('#recaptcha_response_field').val();

            // Full name check
            jQuery('label[for="TUser_full_name"] .invalid-email').remove();
            var patts = /[!@#$%^&*()\{\}<>~`?_\[\]\/:;|"1-9]/g;
            if( jQuery.trim(fullname).match(patts) || jQuery.trim(fullname).match(/\d/g) ){
                jQuery('label[for="TUser_full_name"]').append('<span class="invalid-email">Invalid Full Name.</span>');
                jQuery('#TUser_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if(jQuery.trim(fullname).length == 0){
                jQuery('label[for="TUser_full_name"]').append('<span class="invalid-email">Full Name cannot be blank</span>');
                jQuery('#TUser_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
            }else{
                jQuery('#TUser_full_name').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Email check
            objCheck = USERACCOUNT.validateEmail(email);
            jQuery('label[for="TUser_email"] .invalid-email').remove();
            if(!objCheck){
                jQuery('#TUser_email').removeClass('ok-validate').addClass('hight-light error-validate');
                jQuery('label[for="TUser_email"]').append('<span class="invalid-email">Invalid Email Address</span>');
                validate = false;
            }else{
                objCheck = USERACCOUNT.ajaxObjCheck(email);
                if(!objCheck){
                    jQuery('#TUser_email').removeClass('ok-validate').addClass('hight-light error-validate');
                    jQuery('label[for="TUser_email"]').append('<span class="invalid-email">Email already exists</span>');
                }else{
                    jQuery('#TUser_email').removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }

            // Company check
            /*jQuery('label[for="TUser_company"] .invalid-email').remove();
            if(jQuery.trim(company).length == 0){
                jQuery('label[for="TUser_company"]').append('<span class="invalid-email">Company cannot be blank</span>');
                jQuery('#TUser_company').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(company).length < 2 ){
                jQuery('label[for="TUser_company"]').append('<span class="invalid-email">The Company must from 2 characters</span>');
                jQuery('#TUser_company').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                jQuery('#TUser_company').removeClass('hight-light error-validate').addClass('ok-validate');
            }*/

            // Username Check
            jQuery('label[for="TUser_username"] .invalid-email').remove();
            if(jQuery.trim(username).match(/\s/g)){
                jQuery('label[for="TUser_username"]').append('<span class="invalid-email">Invalid User Name.</span>');
                jQuery('#TUser_username').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if(jQuery.trim(username).length == 0){
                jQuery('label[for="TUser_username"]').append('<span class="invalid-email">User name cannot be blank</span>');
                jQuery('#TUser_username').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(username).length < 4 || jQuery.trim(username).length > 24 ){
                jQuery('label[for="TUser_username"]').append('<span class="invalid-email">The user name must be between 4 and 24 characters</span>');
                jQuery('#TUser_username').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                objCheck = USERACCOUNT.ajaxObjCheck(username);
                if(!objCheck){
                    jQuery('#TUser_username').removeClass('ok-validate').addClass('hight-light error-validate');
                    jQuery('label[for="TUser_username"]').append('<span class="invalid-email">Username already exists</span>');
                }else{
                    jQuery('#TUser_username').removeClass('hight-light error-validate').addClass('ok-validate');
                }
            }

            // Pass Check
            jQuery('label[for="TUser_password"] .invalid-strength').remove();
            jQuery('label[for="TUser_password"] .invalid-email').remove();
            if(jQuery.trim(pass).length == 0){
                jQuery('label[for="TUser_password"]').append('<span class="invalid-email">Password cannot be blank</span>');
                jQuery('#TUser_password').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( jQuery.trim(pass).length < 6 || jQuery.trim(pass).length > 18 ){
                jQuery('label[for="TUser_password"]').append('<span class="invalid-email">Password must be between 6 and 18 characters</span>');
                jQuery('#TUser_password').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                var objCheck = USERACCOUNT.checkValidatePass(jQuery.trim(pass));
                jQuery('#TUser_password').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Re Pass check
            jQuery('label[for="TUser_re_password"] .invalid-email').remove();
            if( jQuery.trim(rePass).length == 0 ){
                jQuery('label[for="TUser_re_password"]').append('<span class="invalid-email">Confirm Password cannot be blank</span>');
                jQuery('#TUser_re_password').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                if(jQuery.trim(pass) != jQuery.trim(rePass)){
                    jQuery('label[for="TUser_re_password"]').append('<span class="invalid-email">The confirm password does not match</span>');
                    jQuery('#TUser_re_password').removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else
                    jQuery('#TUser_re_password').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Check Captcha
            if(jQuery.trim(captcha).length ==0){
                jQuery('.user-field-captcha .alert.alert-error').remove();
                jQuery('.user-field-captcha').append('<div class="alert alert-error">Captcha cannot be blank</div>');
                validate = false;
            }

            // Check Terms
            if(!term){
                jQuery('.user-field-terms .invalid-email').remove();
                jQuery('.user-field-terms').append('<span class="invalid-email">Please check the box to indicate your acceptance.</span>');
                validate = false;
            }
            return validate;
        });
    }

}
jQuery('document').ready(function($){
    USERACCOUNT.init();
});