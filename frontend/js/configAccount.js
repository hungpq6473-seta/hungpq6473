/**
 * Created by Pham Quoc Hung on 6/12/14.
 */

var CONFIGACCOUNTS = {
    init:function(){
        CONFIGACCOUNTS.blurEvents();
        CONFIGACCOUNTS.blurProfile();
        CONFIGACCOUNTS.buildEvent();
        CONFIGACCOUNTS.cancelForm();
        CONFIGACCOUNTS.submitConfigAccount();
        CONFIGACCOUNTS.submitProfile();
        CONFIGACCOUNTS.accountEvents();
    },
    accountEvents:function(){
        var browsers = LAUNCH.browserVersion();
        if(browsers && browsers.name=='Internet Explorer Browser' && browsers.version < 10){
            jQuery('#avatarProfile').text('Browse an image here...');
        }

        // check maxlength Bio
        try {
            jQuery('#UserProfile_bio_user').maxlength({
                maxCharacters: 1500, // Characters limit
                showAlert: false,
                statusTextBefore:'',
                statusText:'characters left'
            });
        }catch(e) {

        }
    },
    blurEvents:function(){
        jQuery('#TUser_password').blur(function(){
           var _this=this, objCheck, pass, re_pass, value, validate=true;
            value = jQuery.trim(jQuery(_this).val());
            jQuery('label[for="TUser_password"] .invalid-field').remove();
            jQuery('label[for="TUser_password"] .invalid-strength').remove();
            if(value.length == 0){
                jQuery('label[for="TUser_password"]').append('<span class="invalid-field">Password cannot be blank.</span>');
                jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( value.length < 6 || value.length > 18 ){
                jQuery('label[for="TUser_password"]').append('<span class="invalid-field">Password must be between 6 and 18 characters.</span>');
                jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                objCheck = LAUNCH.checkValidatePass(value);
                re_pass = jQuery('#TUser_re_password').val();
                if(value == re_pass){
                    jQuery('label[for="TUser_re_password"] .invalid-field').remove();
                    jQuery('#TUser_re_password').removeClass('hight-light error-validate').addClass('ok-validate');
                }
                jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
            }

            return validate;
        });

        jQuery('#TUser_re_password').blur(function(){
            var _this=this, objCheck, pass, re_pass, validate=true;
            pass = jQuery('#TUser_password').val();
            re_pass = jQuery('#TUser_re_password').val();
            jQuery('label[for="TUser_re_password"] .invalid-field').remove();
            if(jQuery.trim(jQuery(_this).val()).length > 0){
                if(pass != re_pass){
                    jQuery('label[for="TUser_re_password"]').append('<span class="invalid-field">The password does not match.</span>');
                    jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else
                    jQuery(_this).removeClass('hight-light error-validate').addClass('ok-validate');
            }else{
                jQuery('label[for="TUser_re_password"]').append('<span class="invalid-field">The password does not match.</span>');
                jQuery(_this).removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }
        });
    },

    submitConfigAccount:function(){
        jQuery('#userAccountId').click(function(){
            var _this=this, objCheck, pass, re_pass, validate=true;
            pass = jQuery.trim(jQuery('#TUser_password').val());
            re_pass = jQuery.trim(jQuery('#TUser_re_password').val());

            // Check Password Validate
            jQuery('label[for="TUser_password"] .invalid-field, label[for="TUser_re_password"] .invalid-field').remove();
            jQuery('label[for="TUser_password"] .invalid-strength').remove();
            if(pass.length == 0){
                jQuery('label[for="TUser_password"]').append('<span class="invalid-field">Password cannot be blank.</span>');
                jQuery('#TUser_password').removeClass('ok-validate').addClass('hight-light error-validate');
            }else if( pass.length < 6 || pass.length > 18 ){
                jQuery('label[for="TUser_password"]').append('<span class="invalid-field">Password must be between 6 and 18 characters.</span>');
                jQuery('#TUser_password').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                objCheck = LAUNCH.checkValidatePass(pass);
                jQuery('#TUser_password').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            // Check Repassword validate
            if(re_pass.length > 0){
                if(pass != re_pass){
                    jQuery('label[for="TUser_re_password"]').append('<span class="invalid-field">The password does not match.</span>');
                    jQuery('#TUser_re_password').removeClass('ok-validate').addClass('hight-light error-validate');
                    validate = false;
                }else
                    jQuery('#TUser_re_password').removeClass('hight-light error-validate').addClass('ok-validate');
            }else{
                jQuery('label[for="TUser_re_password"]').append('<span class="invalid-field">The password does not match.</span>');
                jQuery('#TUser_re_password').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }
            return validate;
        });

    },
    submitProfile:function(){
        jQuery('#userProfileId').click(function(){
            var _this=this,fullname,company,validate=true;
            fullname = jQuery.trim(jQuery('#UserProfile_full_name').val());
            jQuery('label[for="UserProfile_full_name"] .invalid-email').remove();
            var patts = /[!@#$%^&*()\{\}<>~`?_\[\]\/:;|"1-9]/g;
            if( fullname.match(patts) || fullname.match(/\d/g) ){
                jQuery('label[for="UserProfile_full_name"]').append('<span class="invalid-email">Invalid Public Name.</span>');
                jQuery('#UserProfile_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else if(fullname.length == 0){
                jQuery('label[for="UserProfile_full_name"]').append('<span class="invalid-email">Public Name cannot be blank</span>');
                jQuery('#UserProfile_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                jQuery('#UserProfile_full_name').removeClass('hight-light error-validate');
            }

            return validate;
        });

    },
    blurProfile:function(){
        jQuery('#UserProfile_full_name').blur(function(){
            var _this=this,fullname,company,validate=true;
            fullname = jQuery.trim(jQuery('#UserProfile_full_name').val());
            jQuery('label[for="UserProfile_full_name"] .invalid-email').remove();
            var patts = /[!@#$%^&*()\{\}<>~`?_\[\]\/:;|"1-9]/g;
            if( fullname.match(patts) || fullname.match(/\d/g) ){
                jQuery('label[for="UserProfile_full_name"]').append('<span class="invalid-email">Invalid Public Name.</span>');
                jQuery('#UserProfile_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else if(fullname.length == 0){
                jQuery('label[for="UserProfile_full_name"]').append('<span class="invalid-email">Public Name cannot be blank</span>');
                jQuery('#UserProfile_full_name').removeClass('ok-validate').addClass('hight-light error-validate');
                validate = false;
            }else{
                jQuery('#UserProfile_full_name').removeClass('hight-light error-validate').addClass('ok-validate');
            }

            return validate;
        });
    },
    cancelForm:function(){
        jQuery('.button-action-account > a').click(function(e){
            e.preventDefault();
            window.location.href = PARAMS.baseUrl;
        });
    },
    buildEvent:function(){
        /*jQuery("#UserProfile_avatar").on('drop', function (e){
            jQuery('#UserProfile_avatar').change();
            jQuery(this).css('border', '2px dotted #0B85A1');
            e.preventDefault();
            var files = e.originalEvent.dataTransfer.files;

            jQuery(e.originalEvent.dataTransfer.files).each(function(){
                jQuery("#nameFile-Profile").append(this.name).css('display','block');
            });
        });
        jQuery('#UserProfile_avatar').change(function(){
            jQuery('#nameFile-Profile').html(jQuery(this).val()).css('display','block');
        });
         */
        try {
            oHandler = jQuery("#UserProfile_company_type").msDropDown({mainCSS:'dd2'}).data("dd");
            jQuery("#ver").html(jQuery.msDropDown.version);
        }catch(e) {

        }

    },
    media_upload_media:function(obj,callback){
        jQuery(obj).html5Uploader({
            name: "uploadphoto",
            params:{},
            onServerLoadStart:function(e,file){
                jQuery('.load-process').show();
                jQuery('.background-media-proccess-logo').css('width','0%');
                jQuery('.background-media-proccess-logo').text('0%');
            },
            onServerProgress:function(e,file){
                var percentComplete=(e.loaded/e.total)*100;
                jQuery('.background-media-proccess-logo').css('width',percentComplete+'%');
                jQuery('.background-media-proccess-logo').text(Math.round(percentComplete)+'%');
            },
            onServerLoad:function(e,file){
            },
            postUrl: PARAMS.baseUrl+'/user/upload',
            onSuccess:function(e, file, data){
                var getData = jQuery.parseJSON(data);
                jQuery('.load-process').hide();
                jQuery('.photo-avatar-image > a').html(getData.file);
                jQuery('#ytUserProfile_avatar').val(getData.name);
            }
        });
    },
}

jQuery(document).ready(function(){
    CONFIGACCOUNTS.init();
    var obj = jQuery('#UserProfile_avatar');
    var callback = '';
    CONFIGACCOUNTS.media_upload_media(obj,callback);
});