<?php
/* @var $this UserController */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Account');
$this->breadcrumbs=array(
    Yii::t('_yii','Account'),
);
global $language;
?>
<div id="entr-user-account" class="l-container container">

    <div class="account-wrapper-ltr">
        <div class="wrapper-ltr-main">
            <ul>
                <li class="first"><a href="#"><?php echo Yii::t("_yii","Dashboard");?></a></li>
                <li class="m-item"><a href="<?php echo Yii::app()->request->baseUrl.$language; ?>/user/profile"><?php echo Yii::t("_yii","Profile");?></a></li>
                <li class="m-item"><a href="#"><?php echo Yii::t("_yii","Activity");?></a></li>
                <li class="last"><a href="#"><?php echo Yii::t("_yii","Steps");?></a></li>
            </ul>
        </div>
    </div>

    <div class="account-wrapper-rtl">
        <?php if(Yii::app()->user->hasFlash('success')):?>
            <div class="message-success"><?php echo Yii::app()->user->getFlash('success'); ?></div>
        <?php endif; ?>
        <div class="wrapper-rtl-main">

            <div class="form">
                <div class="well-account">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'config-user-account',
                        'enableClientValidation'=>false,
                        'errorMessageCssClass'=>'alert alert-error',
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>

                    <div class="account-right-header">
                        <h3><?php echo Yii::t('_yii','Account');?></h3>
                        <div class="button-action-account">
                            <a href="#"><?php echo Yii::t('_yii','Cancel');?></a>
                            <div class="form-actions">
                                <?php echo CHtml::submitButton('Save', array('id'=>'userAccountId', 'class'=>'btn btn-primary')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="user-account-body">
                        <div class="field-row">
                            <?php echo $form->labelEx($model,'email'); ?>
                            <?php echo $form->textField($model,'email',array('value'=>isset(Yii::app()->user->email)?Yii::app()->user->email:'','disabled'=>'disabled')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </div>

                        <div class="field-row">
                            <?php echo $form->labelEx($model,'username'); ?>
                            <?php echo $form->textField($model,'username',array('value'=>isset(Yii::app()->user->username)?Yii::app()->user->username:'','disabled'=>'disabled')); ?>
                            <?php echo $form->error($model,'username'); ?>
                        </div>

                        <div class="field-row">
                            <?php echo $form->labelEx($model,'password'); ?>
                            <?php echo $form->passwordField($model,'password'); ?>
                            <?php echo $form->error($model,'password'); ?>
                        </div>

                        <div class="field-row">
                            <?php echo $form->labelEx($model,'re_password'); ?>
                            <?php echo $form->passwordField($model,'re_password'); ?>
                            <?php echo $form->error($model,'re_password'); ?>
                        </div>

                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>