<?php
/* @var $this UserController */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Profile');
$this->breadcrumbs=array(
    Yii::t('_yii','Profile'),
);
global $user, $language;
?>
<div id="entr-user-account" class="l-container container">

    <div class="account-wrapper-ltr">
        <div class="wrapper-ltr-main">
            <ul>
                <li class="first"><a href="#"><?php echo Yii::t("_yii","Dashboard");?></a></li>
                <li class="m-item active"><a href="<?php echo Yii::app()->request->baseUrl.$language; ?>/user/profile"><?php echo Yii::t("_yii","Profile");?></a></li>
                <li class="m-item"><a href="#"><?php echo Yii::t("_yii","Activity");?></a></li>
                <li class="last"><a href="#"><?php echo Yii::t("_yii","Steps");?></a></li>
            </ul>
        </div>
    </div>

    <div class="account-wrapper-rtl">
        <?php if(Yii::app()->user->hasFlash('success')):?>
            <div class="message-success"><?php echo Yii::app()->user->getFlash('success'); ?></div>
        <?php endif; ?>
        <div class="wrapper-rtl-main">

            <div class="form">
                <div class="well-account">
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'config-user-account',
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        'enableClientValidation'=>false,
                        'errorMessageCssClass'=>'alert alert-error',
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                    )); ?>

                    <div class="account-right-header">
                        <h3><?php echo Yii::t('_yii','Profile');?></h3>
                        <div class="button-action-account">
                            <a href="#"><?php echo Yii::t('_yii','Cancel');?></a>
                            <div class="form-actions">
                                <?php echo CHtml::submitButton('Save', array('id'=>'userProfileId', 'class'=>'')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="user-account-body">
                        <div class="field-row">
                            <?php echo $form->labelEx($model,'full_name'); ?>
                            <?php echo $form->textField($model,'full_name',array('value'=>isset($user['full_name'])?$user['full_name']:'')); ?>
                            <?php echo $form->error($model,'full_name'); ?>
                        </div>

                        <div class="field-row">
                            <?php echo $form->labelEx($model,'company'); ?>
                            <?php echo $form->textField($model,'company',array('value'=>isset($user['company'])?$user['company']:'')); ?>
                            <?php echo $form->error($model,'company'); ?>
                        </div>

                        <div class="field-row">
                            <?php echo $form->labelEx($model,'company_type'); ?>
                            <?php echo $form->dropDownList($model, 'company_type', $comType, array('class'=>'selectpicker','options'=>array($user['company_type']=>array('selected'=>'selected')))); ?>
                            <?php echo $form->error($model,'company_type'); ?>
                        </div>

                        <div class="field-row">
                            <?php echo $form->labelEx($model,'avatar'); ?>
                            <div class="profile-photo-wrapper">
                                <div class="photo-avatar-image">
                                    <div class="load-process"><span class="background-media-proccess-logo"></span></div>
                                    <a href="#" title="<?php echo isset($user['full_name'])?$user['full_name']:'';?>"><?php echo $avatar;?></a>
                                </div>
                                <div class="field-row-avatar">
                                    <div class="avatar-profile-file">
                                        <?php echo $form->fileField($model, 'avatar', array('value'=>isset($user['avatar'])?$user['avatar']:''));?>
                                        <button type="button" name="avatarProfile" id="avatarProfile"><?php echo Yii::t('_yii','Browse or drag an image here...')?></button>
                                        <span id="nameFile-Profile"></span>
                                    </div>
                                    <div class="intro-photo-profile"><?php echo Yii::t('_yii','Photos should be at least 125px by 125px and in PNG, JPG, or GIF format.')?></div>
                                </div>
                            </div>
                            <?php echo $form->error($model,'avatar'); ?>
                        </div>

                        <div class="field-row bio_user_field">
                            <?php echo $form->labelEx($model,'bio_user'); ?>
                            <?php echo $form->textArea($model, 'bio_user', array('value'=>isset($user['bio_user'])?$user['bio_user']:'', 'maxlength' => 1500, 'rows' => 6, 'cols' => 50)); ?>
                            <?php echo $form->error($model,'bio_user'); ?>
                        </div>
                        <div class="user-field-terms">
                            <?php if($user['keep_private']):?>
                            <?php echo $form->checkBox($model,'keep_private',array('checked'=>'checked')); ?>
                            <?php else:?>
                            <?php echo $form->checkBox($model,'keep_private'); ?>
                            <?php endif;?>
                            <?php echo $form->labelEx($model,'keep_private'); ?>
                            <?php echo $form->error($model,'keep_private'); ?>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>