<?php
/* @var $this UserController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Reset Password');
$this->breadcrumbs=array(
    Yii::t('_yii','Reset Password'),
);
?>
<div id="reset-wrapper">
    <h3><?php echo Yii::t('_yii','Reset Password');?></h3>
	
    <div class="form">
		<div class="well">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'reset-form',
				'enableClientValidation'=>false,
				'enableAjaxValidation'=>false,
				'errorMessageCssClass'=>'alert alert-error',
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

				<?php echo $form->error($model,'password', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<?php echo $form->error($model,'re_password', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<div class="field-row">
					<?php echo $form->labelEx($model,'password'); ?>
					<?php echo $form->passwordField($model,'password'); ?>
				</div>
			
				<div class="field-row">
					<?php echo $form->labelEx($model,'re_password'); ?>
					<?php echo $form->passwordField($model,'re_password'); ?>
				</div>
					
				<p class="user-read-terms">
				</p>
				
				<div class="form-actions">
					<?php echo CHtml::submitButton(Yii::t('_yii','Save New Password'), array('class'=>'btn btn-primary')); ?>
				</div>
				
			<?php $this->endWidget(); ?>
		</div><!-- well -->
    </div><!-- form -->
</div>