<?php if ($result['error'] == 0) : ?>
<script type="text/javascript">
(function(window) {
    if (window.opener) {
        window.opener.location = '<?php echo $result['redirect']; ?>';
        window.close();
    } else {
        window.location = '<?php echo $result['redirect']; ?>';
    }
})(window);
</script>
<?php else : ?>
<h1><?php echo $result['message']; ?></h1>
<?php endif; ?>