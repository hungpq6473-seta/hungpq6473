<div id="step-content">
	
	<div id="step-intro">
		<div class="step-name">
			<span>Step 4: Introduction</span>
			<a href="#">Meet Scoot</a>
		</div><!--.step-name-->
		
		<div class="step-video">
			<?php
				$link = 'https://www.youtube.com/watch?v=PwL0ydaTBp8';
				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/",$link, $matches);
				$idYoutube = $matches[1];
				$src = "https://www.youtube.com/embed/$idYoutube?rel=0";
			?>
			<div class="youtube-video">
				<iframe width="734" height="421" src="<?php echo $src;?>" frameborder="0" allowfullscreen></iframe>
			</div>
			
		</div><!--.step-video-->
		
	</div><!--#step-intro-->
	
	<div id="step-menu">
		<div class="menu-desc">
			<span>Things You Need To Do</span>
		</div><!--.menu-desc-->
		
		<div class="sub-menu">
			<ul>
				<li>
					<a href="#">
						<span>1. Identify Your Target</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>2. Build Your Minimum Viable Pr...</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>3. Develop Partnerships & Distri...</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>4. Engage With Customer, Build...</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>5. Sell! Sell! Sell!</span>
					</a>
				</li>
			</ul>
            
            <div class="advertising">
                <a href="#"><img src="/themes/launch/img/advertising.png"/></a>
            </div>
			
		</div><!--.sub-menu-->
	</div><!--#step-menu-->
</div>