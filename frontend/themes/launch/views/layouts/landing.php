<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php
        global $language;
        Yii::app()->clientScript->scriptMap=array(
            'jquery.js'=>false,
        );
        Yii::app()->getClientScript()->coreScriptPosition = CClientScript::POS_END;
	?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/landing.css" />
</head>
<body>
<div class="main-launch-site">
    <div class="header-wrapper-main">
        <div class="container">
            <div class="row">
                <header class="span12">
                    <div id="header-top" class="row">
                        <div class="span4">
                            <a href="<?php echo Yii::app()->request->baseUrl.$language; ?>/">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="" />
                                <!--<span class="launch-sologan">&ldquo;<?php //echo Yii::t('_yii','5 Steps to Launch and Grow Your Business');?>&rdquo;</span>-->
                            </a>
                        </div>
                        <div class="span7">
                            <div class="line-info-login">
                                <div class="wrapper-line">
                                    <?php if(!Yii::app()->user->isGuest):?>
                                    Hi <?php echo Yii::app()->user->name;?>, welcome back, <a href="<?php echo $language.'/logout';?>"><?php echo Yii::t('_yii','LogOut');?></a>
                                    <?php else:?>
                                    <a href="<?php echo $language.'/login';?>" class="user-login-line"><?php echo Yii::t('_yii','Login');?></a>|
                                    <a href="<?php echo $language.'/user/create';?>" class="user-register-line"><?php echo Yii::t('_yii','Register');?></a>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="navbar">
                                <div class="navbar-inners">
                                    <div class="container">
                                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </a>
                                        <div class="nav-collapse">
                                            <?php $this->widget('zii.widgets.CMenu',array(
                                                'items'=>array(
                                                    array('label'=>Yii::t('_yii','Ask the Expert'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Live Events'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Webinars'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Bookstore'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Special Offers'), 'url'=>array($language.'/#')),
                                                    //array('label'=>Yii::t('_yii','Special Offers').'('.Yii::app()->user->name.')', 'url'=>array($language.'/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                                                    array('label'=>Yii::t('_yii','About'), 'url'=>array($language.'/#'))
                                                ),
                                                'htmlOptions'=>array('class'=>'nav'),
                                            )); ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- navbar -->
                        </div>
                        <div class="span1">
                            <a href="#">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/panic.png" alt="" />
                            </a>
                        </div>
                    </div>

                    <?php $bread=1; if(isset($this->breadcrumbs) && ($bread == 0)):?>
                        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'=>$this->breadcrumbs,
                            'htmlOptions'=>array('class'=>'breadcrumbs breadcrumb'),
                        )); ?><!-- breadcrumbs -->
                    <?php endif?>

                </header>
            </div>
        </div>
    </div><!-- Header site -->
    <div class="main-content">
		<div id="wrapper-nav">
			<div class="container">
				<div class="row">
					<div class="span12">
						<div id="nav">
                            <ul class="parent-ul">
								<li class="first">
									<a href="#">
										<span>Step 1</span>
										<span>Pre-Launch Checklist</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span>Step 2</span>
										<span>Assemble Your Resources</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span>Step 3</span>
										<span>Fuel The Tank</span>
									</a>
								</li>
								<li>
									<a href="#" class="active">
										<span>Step 4</span>
										<span>Get Ready For Take Off</span>
									</a>
									<div class="step-content">
                                        <div class="list-child-wrap">
                                            <div class="top-title">
                                                Things You Need To Do
                                            </div>
                                            <div class="list-child">
                                                <ul class="child-ul">
                                                    <li>
                                                        <span>Step Four - Introduction</span>
                                                        <a class="uncompleted" href="#">Get Started</a>
                                                    </li>
                                                    <li>
                                                        <span>1. Identify Your Target</span>
                                                        <a class="completed" href="#">Completed</a>
                                                    </li>
                                                    <li>
                                                        <span>2. Build Your Minimum Viable Product</span>
                                                        <a class="completed" href="#">Completed</a>
                                                    </li>
                                                    <li>
                                                        <span>3. Develop Partnerships & Distribution</span>
                                                        <a class="uncompleted" href="#">Get Started</a>
                                                    </li>
                                                    <li>
                                                        <span>4. Engage With Customer, Build Relationships</span>
                                                        <a class="uncompleted" href="#">Get Started</a>
                                                    </li>
                                                    <li>
                                                        <span>5. Sell! Sell! Sell!</span>
                                                        <a class="completed" href="#">Completed</a>
                                                    </li>
                                                </ul><!--.ul-child-->
                                                
                                            </div><!--.list-child-->
                                        </div><!--.list-child-wrap-->
                                        <div class="partner">
                                            <div class="top-title">
                                                Partner
                                            </div>
                                        </div><!--.partner-->
                                        <div class="newsletter">
                                            <div class="top-title">
                                                Newsletter
                                            </div>
                                        </div><!--.newsletter-->
									</div><!--.step-content-->
								</li>
								<li class="last">
									<a href="#">
										<span>Step 5</span>
										<span>Launch!</span>
									</a>
								</li>
							</ul>
						</div><!--#nav-->
					</div>
				</div>
			 </div>
		</div><!--#wrapper-nav-->
		
		<div id="wrapper-content">
			<div class="container">
			   <div class="row">
				   <div class="span12">
					   <?php echo $content; ?>
				   </div>
			   </div>
			</div>
		</div><!--#wrapper-content-->
		
		<div class="wrapper-widget">
			<div class="container">
			   <div class="row">
				   <div class="span12">
                       <div class="left-widget">
                            <div class="widget-title">
                                 <span>Launch-o-preneurs</span>
                                 <a href="#">See all</a>
                            </div><!--.widget-title-->
                           
                            <div class="widget-list">
                                <ul>
                                    <li>
										<div class="avatar">
											<a href="#"><img src="/themes/launch/img/mark-avatar.png"/></a>
										</div><!--.avatar-->
										<div class="detail">
											<span>Mark Suster</span>
											<p>The rare twin tornadoes that spun through on Monday night wiped out the town's business district, obliterated its fire station and ground 40 or 50 homes into rubble, Stanton County Sheriff Mike Unger said.</p>
											<a href="#">Meet Mark</a>
										</div><!--.detail-->
                                    </li>
                                    <li>
										<div class="avatar">
											<a href="#"><img src="/themes/launch/img/ron-avatar.png"/></a>
										</div><!--.avatar-->
										<div class="detail">
											<span>Ron Klein</span>
											<p>In southern Wisconsin, a line of thunderstorms that spawned twisters rolled through late Monday and early Tuesday.</p>
											<a href="#">Meet Ron</a>
										</div><!--.detail-->
                                    </li>
                                </ul>
                            </div><!--.widget-list-->
                       </div><!--.left-widget-->
                       
                        <div class="right-widget">
							<div class="widget-title">
								 <span>Tip of the day</span>
								 <a href="#">View more</a>
							</div><!--.widget-title-->
						   
							<div class="widget-list">
								<div class="quote">
									&ldquo;Every single relationship matters - in bussiness and in life.&rdquo;
								</div><!--.quote-->
								<span class="quote-arrow"></span>
								<div class="avatar">
									<a href="#"><img src="/themes/launch/img/scott-avatar.png"/></a>
								</div><!--.avatar-->
								<div class="detail">									
									<span>Scott Duffy</span>
									<p>Scott Duffy twin tornadoes that spun through on Monday night wiped out the town's business district.</p>
									<a href="#">Meet Scott</a>
								</div><!--.detail-->
                            </div><!--.widget-list-->
                       </div><!--.right-widget-->
				   </div>
			   </div>
			</div>
		</div>
    </div><!--Main Content Body -->
    <div class="footer-wrapper-site">
        <div class="container">
            <div class="row">
                <footer class="span12">
                    <div class="row">
                        <div class="span8">
                            <ul>
                                <li><a href="#"><?php echo Yii::t('_yii','Privacy Policy');?></a></li>
                                <li><a href="#"><?php echo Yii::t('_yii','Contact Us');?></a></li>
                                <li><a href="#"><?php echo Yii::t('_yii','Terms of Use');?></a></li>
                                <li><a href="#"><?php echo Yii::t('_yii','Site Map');?></a></li>
                            </ul>
                            <span>&copy;&nbsp;<?php echo date('Y');?> <?php echo Yii::t('_yii','Launch Project, LLC. All rights reserved');?></span>
                        </div>
                        <div class="span4"></div>
                    </div>
                </footer>
            </div><!-- footer -->

        </div><!-- container -->
    </div><!-- Footer Site -->
</div><!-- main site-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/jquery-1.9.1.min.js"><\/script>')</script>
</body>
</html>