<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <?php
    global $language;
    Yii::app()->clientScript->scriptMap=array(
        'jquery.js'=>false,
    );
    Yii::app()->getClientScript()->coreScriptPosition = CClientScript::POS_END;
    ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
</head>
<body>

<div class="main-launch-site">
    <div class="header-wrapper-main">
        <div class="container">
            <div class="row">
                <header class="span12">
                    <div id="header-top" class="row">
                        <div class="span4">
                            <a href="<?php echo Yii::app()->request->baseUrl.$language; ?>/">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logo.png" alt="" />
                                <!--<span class="launch-sologan">&ldquo;<?php //echo Yii::t('_yii','5 Steps to Launch and Grow Your Business');?>&rdquo;</span>-->
                            </a>
                        </div>
                        <div class="span7">
                            <div class="line-info-login">
                                <div class="wrapper-line">
                                    <?php if(!Yii::app()->user->isGuest):?>
                                        Hi <?php echo Yii::app()->user->name;?>, welcome back, <a href="<?php echo $language.'/logout';?>"><?php echo Yii::t('_yii','LogOut');?></a>
                                    <?php else:?>
                                        <a href="<?php echo $language.'/login';?>" class="user-login-line"><?php echo Yii::t('_yii','Login');?></a>|
                                        <a href="<?php echo $language.'/user/create';?>" class="user-register-line"><?php echo Yii::t('_yii','Register');?></a>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="navbar">
                                <div class="navbar-inners">
                                    <div class="container">
                                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </a>
                                        <div class="nav-collapse">
                                            <?php $this->widget('zii.widgets.CMenu',array(
                                                'items'=>array(
                                                    array('label'=>Yii::t('_yii','Ask the Expert'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Live Events'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Webinars'), 'url'=>array($language.'/#')),
                                                    array('label'=>Yii::t('_yii','Bookstore'), 'url'=>array($language.'/bookstore')),
                                                    array('label'=>Yii::t('_yii','Special Offers'), 'url'=>array($language.'/#')),
                                                    //array('label'=>Yii::t('_yii','Special Offers').'('.Yii::app()->user->name.')', 'url'=>array($language.'/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                                                    array('label'=>Yii::t('_yii','About'), 'url'=>array($language.'/#'))
                                                ),
                                                'htmlOptions'=>array('class'=>'nav'),
                                            )); ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- navbar -->
                        </div>
                        <div class="span1">
                            <a href="#">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/panic.png" alt="" />
                            </a>
                        </div>
                    </div>

                    <?php $bread=1; if(isset($this->breadcrumbs) && ($bread == 0)):?>
                        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'=>$this->breadcrumbs,
                            'htmlOptions'=>array('class'=>'breadcrumbs breadcrumb'),
                        )); ?><!-- breadcrumbs -->
                    <?php endif?>

                </header>
            </div>
        </div>
    </div><!-- Header site -->
    <div class="line-head-book">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="line-head-book-content">
                        <span>"<?php echo Yii::t('_yii','Someone is sitting in the shade today because someone planted a tree a long time ago.');?>"</span>&nbsp;<strong>- Warren Buffett</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content">
        <div class="main-left-content">

        </div>
        <div class="main-right-content">

        </div>
        <div class="container">
            <div class="row">
                <div class="span12">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
    </div><!--Main Content Body -->
    <div class="footer-wrapper-site">
        <div class="container">
            <div class="row">
                <footer class="span12">
                    <div class="row">
                        <div class="span8">
                            <ul>
                                <li><a href="#"><?php echo Yii::t('_yii','Privacy Policy');?></a></li>
                                <li><a href="#"><?php echo Yii::t('_yii','Contact Us');?></a></li>
                                <li><a href="#"><?php echo Yii::t('_yii','Terms of Use');?></a></li>
                                <li><a href="#"><?php echo Yii::t('_yii','Site Map');?></a></li>
                            </ul>
                            <span>&copy;&nbsp;<?php echo date('Y');?> <?php echo Yii::t('_yii','Launch Project, LLC. All rights reserved');?></span>
                        </div>
                        <div class="span4"></div>
                    </div>
                </footer>
            </div><!-- footer -->

        </div><!-- container -->
    </div><!-- Footer Site -->
</div><!-- main site-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/jquery-1.9.1.min.js"><\/script>')</script>
</body>
</html>