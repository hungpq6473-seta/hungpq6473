<?php
/* @var $this UserController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Forgot Password');
$this->breadcrumbs=array(
    Yii::t('_yii','Forgot Password'),
);
?>
<div id="forgot-wrapper">
    <h3><?php echo Yii::t('_yii','Forgot Password');?></h3>
	
    <div class="form">
		<div class="hint-wrapper">
			<label class="bold"><?php echo Yii::t('_yii', 'Don\'t have an account?');?></label>
			<label id="faint">
				<?php echo Yii::t('_yii', 'Signing up is quick and easy!');?>
				<a href="<?php echo Yii::app()->getBaseUrl(true);?>/user/create" class="create-link"><?php echo Yii::t('_yii', 'Create an account');?></a>
			</label>
		</div>
		
		
		
		<div class="well">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'forgot-form',
				'enableClientValidation'=>false,
				'enableAjaxValidation'=>false,
				'errorMessageCssClass'=>'alert alert-error',
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

				<?php //echo $form->errorSummary($model,NULL,NULL,$htmlOptions=array('class'=>'alert alert-error')); ?>
				<?php echo $form->error($model,'username', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<?php echo $form->error($model,'email', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<?php echo $form->error($model,'checkCapt', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<div class="field-row">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username'); ?>
				</div>
			
				<div class="field-row center">
					<label><?php echo Yii::t('_yii', '-or-');?></label>
				</div>
			
				<div class="field-row">
					<?php echo $form->labelEx($model,'email'); ?>
					<?php echo $form->textField($model,'email'); ?>
				</div>
			
				<div class="user-field-captcha">
					<?php
					$this->widget('application.extensions.recaptcha.EReCaptcha',
						array('attribute'=>'checkCapt',
							'theme'=>'red', 'language'=>'en_US',
							'publicKey'=>Yii::app()->params['recaptcha']['publicKey']));
					?>
				</div>
			
				<p class="user-read-terms">
				</p>
				
				<div class="form-actions">
					<?php echo CHtml::submitButton(Yii::t('_yii','Reset Password'), array('class'=>'btn btn-primary')); ?>
				</div>
				
			<?php $this->endWidget(); ?>
		</div><!-- well -->
    </div><!-- form -->
</div>