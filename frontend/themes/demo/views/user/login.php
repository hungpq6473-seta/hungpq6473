<?php
/* @var $this UserController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Login');
$this->breadcrumbs=array(
    Yii::t('_yii','Login'),
);
?>
<div id="login-wrapper">
    <h3><?php echo Yii::t('_yii','Log In to Launch!');?></h3>
	
    <div class="form">
		<div class="hint-wrapper">
			<label class="bold"><?php echo Yii::t('_yii', 'Don\'t have an account?');?></label>
			<label id="faint">
				<?php echo Yii::t('_yii', 'Signing up is quick and easy!');?>
				<a href="<?php echo Yii::app()->getBaseUrl(true);?>/user/create" class="create-link"><?php echo Yii::t('_yii', 'Create an account');?></a>
			</label>
		</div>
		
		
		
		<div class="well">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'enableClientValidation'=>false,
				'enableAjaxValidation'=>true,
				'errorMessageCssClass'=>'alert alert-error',
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

				<?php //echo $form->errorSummary($model,NULL,NULL,$htmlOptions=array('class'=>'alert alert-error')); ?>
				<?php echo $form->error($model,'username', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<?php echo $form->error($model,'password', array('class'=>'alert alert-error error-validate hight-light')); ?>
				<div class="field-row">
					<?php echo $form->labelEx($model,'username'); ?>
					<?php echo $form->textField($model,'username', array('tabindex'=>1)); ?>
				</div>

				<div class="field-row">
					<?php echo $form->labelEx($model,'password', array('class'=>'password-label')); ?>
					<?php echo Chtml::link(Yii::t('_yii', 'Forgot Password?'), Yii::app()->getBaseUrl(true).'/user/forgot', array('class'=>'forgot-link'));?>
					<?php echo $form->passwordField($model,'password', array('tabindex'=>2)); ?>
				</div>

				<div class="user-field-terms">
					<?php echo $form->checkBox($model,'rememberMe',array('checked'=>true, 'tabindex'=>3)); ?>
					<?php echo $form->label($model,'rememberMe'); ?>
				</div>
				<p class="user-read-terms">
				</p>
				
				<div class="form-actions">
					<?php echo CHtml::submitButton(Yii::t('_yii','Log In'), array('class'=>'btn btn-primary')); ?>
				</div>
				
				<label class="bold"><?php echo Yii::t('_yii', 'Log In with social');?></label>
				
				<?php //if(ST_DOMAIN != ST_CUR_DOMAIN ) : ?>
					<div class="field-row social">
						<a class="js-openpopup facebook" href="<?php echo Yii::app()->getBaseUrl(true)?>/oauth/index?provider=facebook"><span><?php echo Yii::t('_yii', 'Log in with Facebook')?></span></a>
						<a class="js-openpopup twitter" href="<?php echo Yii::app()->getBaseUrl(true)?>/oauth/index?provider=twitter"><span><?php echo Yii::t('_yii', 'Log in with Twitter')?></span></a>
						<a class="js-openpopup google" href="<?php echo 'http://'.$domain?>/oauth/index?provider=google"><span><?php echo Yii::t('_yii', 'Log in with Google')?></span></a>
					</div>
				<?php //endif; ?>
			<?php $this->endWidget(); ?>
		</div><!-- well -->
    </div><!-- form -->
</div>