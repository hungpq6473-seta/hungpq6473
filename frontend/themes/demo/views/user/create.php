<?php
/* @var $this UserController */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('_yii','Create a New Account');
$this->breadcrumbs=array(
    Yii::t('_yii','Create a New Account'),
);
?>
<div id="create-user-account">
    <h3><?php echo Yii::t('_yii','Create a New Account');?></h3>

    <div class="form">
        <div class="well">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'create-user-form',
                'enableClientValidation'=>false,
                'errorMessageCssClass'=>'alert alert-error',
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>
            <?php //echo $form->errorSummary($model,NULL,NULL,$htmlOptions=array('class'=>'alert alert-error')); ?>
            <div class="field-row">
            <?php echo $form->labelEx($model,'full_name'); ?>
            <?php echo $form->textField($model,'full_name'); ?>
            <?php echo $form->error($model,'full_name'); ?>
            </div>

            <div class="field-row">
            <?php echo $form->labelEx($model,'company'); ?>
            <?php echo $form->textField($model,'company'); ?>
            <?php echo $form->error($model,'company'); ?>
            </div>

            <div class="field-row">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email'); ?>
            <?php echo $form->error($model,'email'); ?>
            </div>

            <div class="field-row">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
            </div>

            <div class="field-row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password'); ?>
            </div>

            <div class="field-row">
            <?php echo $form->labelEx($model,'re_password'); ?>
            <?php echo $form->passwordField($model,'re_password'); ?>
            <?php echo $form->error($model,'re_password'); ?>
            </div>

            <div class="field-row">
            <?php $accountType = array('1'=>Yii::t('_yii','Entrpreneur'),'2'=>Yii::t('_yii','Content Provider'));?>
            <?php echo $form->labelEx($model,'account_type'); ?>
            <?php echo $form->radioButtonList($model,'account_type',$accountType,array('separator'=>' ')); ?>
            <?php echo $form->error($model,'account_type'); ?>
            </div>
            <div class="user-field-captcha">
                <?php
                //echo $form->labelEx($model, 'validation');
                $this->widget('application.extensions.recaptcha.EReCaptcha',
                    array('attribute'=>'validateCapt',
                        'theme'=>'red', 'language'=>'en_US',
                        'publicKey'=>Yii::app()->params['recaptcha']['publicKey']));
                echo $form->error($model, 'validateCapt');
                ?>
            </div>

            <div class="user-field-terms">
                <?php echo $form->checkBox($model,'user_terms'); ?>
                <?php echo $form->labelEx($model,'user_terms'); ?>
                <?php echo $form->error($model,'user_terms'); ?>
            </div>
            <p class="user-read-terms">
                <?php echo Yii::t('_yii','By clicking on \'I Agree\' you are confirming that you have read, understood, and accept the Terms of Service Privacy Policy');?>
            </p>
            <div class="form-actions">
                <?php echo CHtml::submitButton(Yii::t('_yii','Sign Up'), array('class'=>'btn btn-primary','id'=>'registerId')); ?>
            </div>
			
			<label class="bold"><?php echo Yii::t('_yii', 'Sign Up with social');?></label>

			<?php //if(ST_DOMAIN != ST_CUR_DOMAIN ) : ?>
				<div class="field-row social">
					<a class="js-openpopup facebook" href="<?php echo Yii::app()->getBaseUrl(true)?>/user/oauth?provider=facebook"><span><?php echo Yii::t('_yii', 'Sign up with Facebook')?></span></a>
					<a class="js-openpopup twitter" href="<?php echo Yii::app()->getBaseUrl(true)?>/user/oauth?provider=twitter"><span><?php echo Yii::t('_yii', 'Sign up with Twitter')?></span></a>
					<a class="js-openpopup google" href="<?php echo 'http://'.$domain?>/user/oauth?provider=google"><span><?php echo Yii::t('_yii', 'Sign up with Google')?></span></a>
				</div>
			<?php //endif; ?>

            <?php $this->endWidget(); ?>
        </div>
    </div>

</div>