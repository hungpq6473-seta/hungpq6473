<?php
/* @var $this UserController */

$this->breadcrumbs=array(
    'Create Partner',
);
?>

<h1>Create Partner</h1>

<div class="form">
    <div class="well">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'create-tenant-form',
            'enableClientValidation'=>true,
            'errorMessageCssClass'=>'alert alert-error',
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
        )); ?>
        <p class="note">Fields with <span class="required">*</span> are required.</p>
        <?php echo $form->errorSummary($model,NULL,NULL,$htmlOptions=array('class'=>'alert alert-error')); ?>

        <?php echo $form->labelEx($model,'full_name'); ?>
        <?php echo $form->textField($model,'full_name'); ?>
        <?php echo $form->error($model,'full_name'); ?>

        <?php echo $form->labelEx($model,'dbu'); ?>
        <?php echo $form->textField($model,'dbu'); ?>
        <?php echo $form->error($model,'dbu'); ?>

        <?php echo $form->labelEx($model,'e_dbpwd'); ?>
        <?php echo $form->passwordField($model,'e_dbpwd'); ?>
        <?php echo $form->error($model,'e_dbpwd'); ?>

        <?php echo $form->labelEx($model,'business_name'); ?>
        <?php echo $form->textField($model,'business_name'); ?>
        <?php echo $form->error($model,'business_name'); ?>

        <?php echo $form->labelEx($model,'domain'); ?>
        <div>
            <span>http://</span>
            <?php echo $form->textField($model,'domain',array('size'=>30,'class'=>'input-small')); ?>
            <span>.launch.alo</span>
        </div>
        <?php echo $form->error($model,'domain'); ?>

        <?php if(CCaptcha::checkRequirements()): ?>
            <?php echo $form->labelEx($model,'verifyCode'); ?>
            <div>
                <?php $this->widget('CCaptcha'); ?><br />
                <?php echo $form->textField($model,'verifyCode'); ?>
            </div>
            <div class="hint">Please enter the letters as they are shown in the image above.
                <br/>Letters are not case-sensitive.</div>
            <?php echo $form->error($model,'verifyCode'); ?>
        <?php endif; ?>

        <div class="form-actions">
            <?php echo CHtml::submitButton('Create', array('class'=>'btn btn-primary')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>