<?php
/* @var $this EventController */

$this->breadcrumbs=array(
	'Live Events',
);
?>
<h1>Live Events</h1>

<div id="live-event-data">
    <div class="live-event-wrapper">
        <ul>
            <?php foreach($events as $key=>$item):?>
            <li>
                <h3><?php echo $item['title_event'];?></h3>
                <div class="content-live-event">
                    <?php echo $item['description_event'];?>
                </div>
            </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
